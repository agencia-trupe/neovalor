import AjaxSetup from "./AjaxSetup";
import MobileToggle from "./MobileToggle";

AjaxSetup();
MobileToggle();

$("section.home").on("load", function () {});

$(document).ready(function () {
  // TODAS AS PAGINAS - chat whatsapp floating
  $("#chatWhatsapp").floatingWhatsApp({
    phone: "+55" + whatsappNumero,
    popupMessage: "Seja muito bem-vindo(a) à Neovalor. Como podemos ajudar?",
    showPopup: true,
    autoOpen: false,
    headerTitle:
      '<div class="img-neovalor"><img src="' +
      imgMarcaWhatsapp +
      '" alt=""></div>' +
      '<div class="textos"><p class="titulo">Neovalor</p><p class="frase">Responderemos o mais breve possível.</p></div>',
    headerColor: "#37B94A",
    size: "60px",
    position: "right",
    linkButton: true,
  });

  // HOME - banners
  $(".banners").cycle({
    slides: ".banner",
    fx: "fade",
    speed: 800,
    timeout: 8000,
    pager: ".cycle-pager",
  });

  // HOME - exibição de cycle-pager quando mouse está no banner
  $(".banners")
    .mouseenter(function () {
      $(".cycle-pager").css("display", "flex");
    })
    .mouseleave(function () {
      $(".cycle-pager").css("display", "none");
    });

  // HOME - efeito contadores
  if (window.location.href == routeHome) {
    var contadoresHome = $("section.home .contador .numero");
    var executou = false;

    $(document).scroll(function () {
      if (
        !executou &&
        $(window).scrollTop() > $("#contadoresHome").offset().top / 2
      ) {
        contadoresHome.each(function (element) {
          var contador = $(this);
          var max = contador.parent().children().html();
          var incrementContador = function (i, max) {
            if (i > max) return;
            setTimeout(function () {
              contador.html(i);
              incrementContador(i + 1, max);
            }, 200);
          };
          incrementContador(0, max);
        });
        executou = true;
      }
    });
  }

  // CONSULTORIA - projetos clique active
  $("section.consultoria .itens-projetos a.projeto").click(function (e) {
    e.preventDefault();
    $(this).toggleClass("active");
  });

  // CONSULTORIA - efeito persiana (informacoes-extras)
  $("section.consultoria .informacao h2.titulo").click(function (e) {
    e.preventDefault();

    $("section.consultoria .informacao.active").removeClass("active");
    $(this).parent().toggleClass("active");
  });

  // CLIENTES E PROJETOS - efeito persiana (experiencias)
  $("section.clientes-e-projetos .grupo .fechado").click(function (e) {
    e.preventDefault();
    $("section.clientes-e-projetos .grupo.active").removeClass("active");
    $(this).parent().toggleClass("active");
  });

  // CLIENTES E PROJETOS - clique em fechar remove active
  $("section.clientes-e-projetos").on(
    "click",
    ".grupo.active a.fechar",
    function (e) {
      e.preventDefault();
      $("section.clientes-e-projetos .grupo.active").removeClass("active");
    }
  );

  // CLIENTES E PROJETOS - efeito contadores
  var contadoresClientesProjetos = $(
    "section.clientes-e-projetos .contador .numero"
  );

  contadoresClientesProjetos.each(function (element) {
    var contador = $(this);
    var max = contador.parent().children().html();
    var incrementContador = function (i, max) {
      if (i > max) return;
      setTimeout(function () {
        contador.html(i);
        incrementContador(i + 1, max);
      }, 200);
    };
    incrementContador(0, max);
  });

  // IMERSÃO - imersões clique active
  $("section.imersao .imersoes a.imersao").click(function (e) {
    e.preventDefault();
    $(this).toggleClass("active");
  });

  // IMERSÃO - efeito persiana (informacoes-extras)
  $("section.imersao .informacao h2.titulo").click(function (e) {
    e.preventDefault();

    $("section.imersao .informacao.active").removeClass("active");
    $(this).parent().toggleClass("active");
  });

  // NEOLABX - imersões clique active
  $("section.neolabx .inovacao a.link-inovacao").click(function (e) {
    e.preventDefault();
    $(this).toggleClass("active");
  });

  // NEOLABX - grid rede de consultores
  $(".neolabx .rede-consultores .consultor:nth-child(2n)").css("margin-right", "0");
  $(".masonry-grid").masonryGrid({
    columns: 2,
  });

  // BLOG - btn ver mais
  var itensBlog = $("section.blog .artigo");
  var spliceItensQuantidade = 4;

  if (itensBlog.length <= spliceItensQuantidade) {
    $(".btn-mais-artigos").hide();
  }

  var setDivArtigos = function () {
    var spliceItens = itensBlog.splice(0, spliceItensQuantidade);
    $("section.blog .artigos").append(spliceItens);
    $(spliceItens).show();
    if (itensBlog.length <= 0) {
      $(".btn-mais-artigos").hide();
    }
  };

  $(".btn-mais-artigos").click(function () {
    setDivArtigos();
  });

  $("section.blog .artigo").hide();
  setDivArtigos();

  // removendo margin-right dos itens finais das linhas
  $(
    "section.consultoria .projeto:nth-child(3n), section.clientes-e-projetos .treinamento:nth-child(3n)"
  ).css("margin-right", "0");

  if ($(window).width() >= 1250) {
    $("section.neovalor .expertise:nth-child(3n)").css("margin-right", "0");
  }

  // LOCALSTORAGE - actives links
  $("section.neovalor .expertise").click(function (e) {
    e.preventDefault();
    localStorage.clear();
    localStorage.setItem("projetoSlug", $(this).attr("id"));
    window.location.href = $(this).children().attr("href");
  });

  $("header .link-nav, footer .link-nav").click(function () {
    localStorage.clear();
  });

  var projetoSlug = localStorage.getItem("projetoSlug");

  if (projetoSlug) {
    $("section.consultoria .projeto#" + projetoSlug).addClass("active");
  }

  // AVISO DE COOKIES
  $(".aviso-cookies").hide();

  if (window.location.href == routeHome) {
    $(".aviso-cookies").show();

    $(".aceitar-cookies").click(function () {
      var url = window.location.origin + "/aceite-de-cookies";

      $.ajax({
        type: "POST",
        url: url,
        success: function (data, textStatus, jqXHR) {
          $(".aviso-cookies").hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR, textStatus, errorThrown);
        },
      });
    });
  }

  // máscara telefone
  $(".input-telefone").mask("(00) 000000000");
});
