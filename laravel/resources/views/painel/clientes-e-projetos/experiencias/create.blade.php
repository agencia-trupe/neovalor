@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>CLIENTES E PROJETOS - Experiências |</small> Adicionar Experiência</h2>
</legend>

{!! Form::open(['route' => 'painel.experiencias.store', 'files' => true]) !!}

@include('painel.clientes-e-projetos.experiencias.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection