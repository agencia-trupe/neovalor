@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('grupo_id', 'Grupo de Experiências') !!}
    {!! Form::select('grupo_id', $grupos , old('grupo_id'), ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('periodo', 'Período') !!}
    {!! Form::text('periodo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.experiencias.index') }}" class="btn btn-default btn-voltar">Voltar</a>