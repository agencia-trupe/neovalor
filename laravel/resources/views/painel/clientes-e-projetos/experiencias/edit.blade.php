@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>CLIENTES E PROJETOS - Experiências |</small> Editar Experiência</h2>
</legend>

{!! Form::model($experiencia, [
'route' => ['painel.experiencias.update', $experiencia->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.clientes-e-projetos.experiencias.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection