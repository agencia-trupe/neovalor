@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        CLIENTES E PROJETOS - Experiências
        <a href="{{ route('painel.experiencias.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Experiência</a>
    </h2>
</legend>

<h4 class="titulo-filtro">Grupos:</h4>
<div class="lista-filtro">
    <a href="{{ route('painel.experiencias.index') }}" class="item">Todos</a>
    @foreach($grupos as $grupo)
    <a href="{{ route('painel.experiencias.index', ['grupo' => $grupo->id]) }}" class="item {{ (isset($_GET['grupo']) && $_GET['grupo'] == $grupo->id) ? 'active' : '' }}">{{ $grupo->titulo }}</a>
    @endforeach
</div>

@if(!count($experiencias))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="experiencias">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Grupo</th>
            <th>Título da Experiência</th>
            <th>Período</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($experiencias as $experiencia)
        <tr class="tr-row" id="{{ $experiencia->id }}">
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>
            @php
            $grupo = $grupos->find($experiencia->grupo_id)->titulo;
            @endphp
            <td>{{ $grupo }}</td>
            <td>{{ $experiencia->titulo }}</td>
            <td>{{ $experiencia->periodo }}</td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.experiencias.destroy', $experiencia->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.experiencias.edit', $experiencia->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection