@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('capa', 'Capa (Imagem Inicial da Página)') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/clientes-e-projetos/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('frase', 'Frase Inicial') !!}
    {!! Form::text('frase', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_treinamentos', 'Texto Treinamentos') !!}
    {!! Form::textarea('texto_treinamentos', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}