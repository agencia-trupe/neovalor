@extends('painel.common.template')

@section('content')

<legend>
    <h2>CLIENTES E PROJETOS - Dados da página</h2>
</legend>

{!! Form::model($registro, [
'route' => ['painel.clientes-e-projetos.update', $registro->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.clientes-e-projetos.dados.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection