@include('painel.common.flash')

<div class="row">

    <div class="col-md-8">
        <div class="form-group">
            {!! Form::label('item1', 'Item 1') !!}
            {!! Form::text('item1', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('quantidade1', 'Quantidade 1') !!}
            {!! Form::number('quantidade1', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-8">
        <div class="form-group">
            {!! Form::label('item2', 'Item 2') !!}
            {!! Form::text('item2', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('quantidade2', 'Quantidade 2') !!}
            {!! Form::number('quantidade2', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-8">
        <div class="form-group">
            {!! Form::label('item3', 'Item 3') !!}
            {!! Form::text('item3', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('quantidade3', 'Quantidade 3') !!}
            {!! Form::number('quantidade3', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-8">
        <div class="form-group">
            {!! Form::label('item4', 'Item 4') !!}
            {!! Form::text('item4', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('quantidade4', 'Quantidade 4') !!}
            {!! Form::number('quantidade4', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-8">
        <div class="form-group">
            {!! Form::label('item5', 'Item 5') !!}
            {!! Form::text('item5', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('quantidade5', 'Quantidade 5') !!}
            {!! Form::number('quantidade5', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-8">
        <div class="form-group">
            {!! Form::label('item6', 'Item 6') !!}
            {!! Form::text('item6', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('quantidade6', 'Quantidade 6') !!}
            {!! Form::number('quantidade6', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}