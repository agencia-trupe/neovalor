@extends('painel.common.template')

@section('content')

<legend>
    <h2>CLIENTES E PROJETOS - Contadores</h2>
</legend>

{!! Form::model($registro, [
'route' => ['painel.clientes-e-projetos.contadores.update', $registro->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.clientes-e-projetos.contadores.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection