@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Logo Empresa') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/treinamentos/'.$treinamento->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('empresa', 'Empresa') !!}
    {!! Form::text('empresa', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('ano', 'Ano') !!}
    {!! Form::text('ano', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('tipo', 'Tipo') !!}
    {!! Form::text('tipo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título do Treinamento') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('consultor', 'Consultor') !!}
    {!! Form::text('consultor', null, ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.treinamentos.index') }}" class="btn btn-default btn-voltar">Voltar</a>