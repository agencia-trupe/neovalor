@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        CLIENTES E PROJETOS - Treinamentos
        <a href="{{ route('painel.treinamentos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Treinamento</a>
    </h2>
</legend>


@if(!count($treinamentos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="treinamentos">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Logo Empresa</th>
            <th>Ano</th>
            <th>Título</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($treinamentos as $treinamento)
        <tr class="tr-row" id="{{ $treinamento->id }}">
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>
            <td><img src="{{ asset('assets/img/treinamentos/'.$treinamento->imagem) }}" style="width: 100%; max-width:100px;" alt=""></td>
            <td>{{ $treinamento->ano }}</td>
            <td>{{ $treinamento->titulo }}</td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.treinamentos.destroy', $treinamento->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.treinamentos.edit', $treinamento->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection