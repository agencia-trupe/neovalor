@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>CLIENTES E PROJETOS - Treinamentos |</small> Adicionar Treinamento</h2>
</legend>

{!! Form::open(['route' => 'painel.treinamentos.store', 'files' => true]) !!}

@include('painel.clientes-e-projetos.treinamentos.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection