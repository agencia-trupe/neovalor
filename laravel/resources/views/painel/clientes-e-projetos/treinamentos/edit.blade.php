@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>CLIENTES E PROJETOS - Treinamentos |</small> Editar Treinamento</h2>
</legend>

{!! Form::model($treinamento, [
'route' => ['painel.treinamentos.update', $treinamento->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.clientes-e-projetos.treinamentos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection