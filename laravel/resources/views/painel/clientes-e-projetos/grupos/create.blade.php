@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>CLIENTES E PROJETOS - Grupos de Experiências |</small> Adicionar Grupo</h2>
</legend>

{!! Form::open(['route' => 'painel.grupos-experiencias.store', 'files' => true]) !!}

@include('painel.clientes-e-projetos.grupos.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection