@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>CLIENTES E PROJETOS - Grupos de Experiências |</small> Editar Grupo</h2>
</legend>

{!! Form::model($grupo, [
'route' => ['painel.grupos-experiencias.update', $grupo->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.clientes-e-projetos.grupos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection