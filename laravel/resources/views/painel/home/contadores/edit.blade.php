@extends('painel.common.template')

@section('content')

<legend>
    <h2>HOME - Contadores</h2>
</legend>

{!! Form::model($registro, [
'route' => ['painel.home.contadores.update', $registro->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.home.contadores.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection