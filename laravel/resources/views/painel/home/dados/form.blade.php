@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto_sobre', 'Texto Sobre (mini currículo)') !!}
    {!! Form::textarea('texto_sobre', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_projetos', 'Texto Clientes e Projetos') !!}
    {!! Form::textarea('texto_projetos', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('frase_cx1', 'Caixa 01 - Frase') !!}
            {!! Form::text('frase_cx1', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('link_cx1', 'Caixa 01 - Link') !!}
            {!! Form::text('link_cx1', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('frase_cx2', 'Caixa 02 - Frase') !!}
            {!! Form::text('frase_cx2', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('link_cx2', 'Caixa 02 - Link') !!}
            {!! Form::text('link_cx2', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('frase_cx3', 'Caixa 03 - Frase') !!}
            {!! Form::text('frase_cx3', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('link_cx3', 'Caixa 03 - Link') !!}
            {!! Form::text('link_cx3', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('imagem_bg', 'Imagem - Clientes e Projetos') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/home/'.$registro->imagem_bg) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_bg', ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}