@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>BLOG - Artigos |</small> Editar Artigo</h2>
</legend>

{!! Form::model($artigo, [
'route' => ['painel.blog.artigos.update', $artigo->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.blog.artigos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection