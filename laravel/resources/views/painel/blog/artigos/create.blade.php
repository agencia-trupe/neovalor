@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>BLOG - Artigos |</small> Adicionar Artigo</h2>
</legend>

{!! Form::open(['route' => 'painel.blog.artigos.store', 'files' => true]) !!}

@include('painel.blog.artigos.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection