@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        BLOG - Artigos
        <a href="{{ route('painel.blog.artigos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Artigo</a>
    </h2>
</legend>

<h4 class="titulo-filtro">Categorias:</h4>
<div class="lista-filtro">
    <a href="{{ route('painel.blog.artigos.index') }}" class="item">Todos</a>
    @foreach($categorias as $categoria)
    <a href="{{ route('painel.blog.artigos.index', ['categoria' => $categoria->id]) }}" class="item {{ (isset($_GET['categoria']) && $_GET['categoria'] == $categoria->id) ? 'active' : '' }}">{{ $categoria->titulo }}</a>
    @endforeach
</div>

@if(!count($artigos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="blog_artigos">
    <thead>
        <tr>
            <th>Data</th>
            <th>Capa</th>
            <th>Título</th>
            <th>Categoria</th>
            <th>NeoLabX</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($artigos as $artigo)
        <tr class="tr-row" id="{{ $artigo->id }}">
            <td>{{ strftime("%d/%m/%Y", strtotime($artigo->data)) }}</td>
            <td><img src="{{ asset('assets/img/blog/'.$artigo->capa) }}" style="width: 100%; max-width:80px;" alt=""></td>
            <td>{{ $artigo->titulo }}</td>
            @php
            $categoria = $categorias->find($artigo->categoria_id)->titulo;
            @endphp
            <td>{{ $categoria }}</td>
            <td style="text-align: center;">
                <span class="glyphicon {{ $artigo->neolabx ? 'text-success glyphicon-ok' : 'text-danger glyphicon-remove' }}"></span>
            </td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.blog.artigos.destroy', $artigo->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.blog.artigos.edit', $artigo->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection