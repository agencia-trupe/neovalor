<ul class="nav navbar-nav">

    <li class="dropdown @if(Tools::routeIs(['painel.banners*', 'painel.home*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Home <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.home.index')) class="active" @endif>
                <a href="{{ route('painel.home.index') }}">Dados</a>
            </li>
            <li @if(Tools::routeIs('painel.banners*')) class="active" @endif>
                <a href="{{ route('painel.banners.index') }}">Banners</a>
            </li>
            <li @if(Tools::routeIs('painel.home.contadores*')) class="active" @endif>
                <a href="{{ route('painel.home.contadores.index') }}">Contadores</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(Tools::routeIs(['painel.empresa*', 'painel.expertises*', 'painel.curriculo*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Neovalor <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.empresa*')) class="active" @endif>
                <a href="{{ route('painel.empresa.index') }}">Dados</a>
            </li>
            <li @if(Tools::routeIs('painel.expertises*')) class="active" @endif>
                <a href="{{ route('painel.expertises.index') }}">Expertises</a>
            </li>
            <li @if(Tools::routeIs('painel.curriculo*')) class="active" @endif>
                <a href="{{ route('painel.curriculo.index') }}">Currículo</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(Tools::routeIs(['painel.consultoria*', 'painel.projetos*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Consultoria <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs(['painel.consultoria.index', 'painel.consultoria.create' , 'painel.consultoria.edit' ])) class="active" @endif>
                <a href="{{ route('painel.consultoria.index') }}">Dados</a>
            </li>
            <li @if(Tools::routeIs('painel.projetos*')) class="active" @endif>
                <a href="{{ route('painel.projetos.index') }}">Projetos</a>
            </li>
            <li @if(Tools::routeIs('painel.consultoria.informacoes*')) class="active" @endif>
                <a href="{{ route('painel.consultoria.informacoes.index') }}">Informações Extras</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(Tools::routeIs(['painel.clientes-e-projetos*', 'painel.grupos-experiencias*', 'painel.experiencias*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Clientes e Projetos <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.clientes-e-projetos.index')) class="active" @endif>
                <a href="{{ route('painel.clientes-e-projetos.index') }}">Dados</a>
            </li>
            <li @if(Tools::routeIs('painel.clientes-e-projetos.contadores*')) class="active" @endif>
                <a href="{{ route('painel.clientes-e-projetos.contadores.index') }}">Contadores</a>
            </li>
            <li @if(Tools::routeIs('painel.grupos-experiencias*')) class="active" @endif>
                <a href="{{ route('painel.grupos-experiencias.index') }}">Grupos de Experiências</a>
            </li>
            <li @if(Tools::routeIs('painel.experiencias*')) class="active" @endif>
                <a href="{{ route('painel.experiencias.index') }}">Experiências</a>
            </li>
            <li @if(Tools::routeIs('painel.treinamentos*')) class="active" @endif>
                <a href="{{ route('painel.treinamentos.index') }}">Treinamentos</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(Tools::routeIs(['painel.imersao*', 'painel.imersoes*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Educação Corporativa <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.imersao.index')) class="active" @endif>
                <a href="{{ route('painel.imersao.index') }}">Dados</a>
            </li>
            <li @if(Tools::routeIs('painel.imersoes*')) class="active" @endif>
                <a href="{{ route('painel.imersoes.index') }}">Imersões</a>
            </li>
            <li @if(Tools::routeIs('painel.imersao.informacoes*')) class="active" @endif>
                <a href="{{ route('painel.imersao.informacoes.index') }}">Informações Extras</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(Tools::routeIs(['painel.neolabx*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">NeoLabX <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.neolabx.index')) class="active" @endif>
                <a href="{{ route('painel.neolabx.index') }}">Dados</a>
            </li>
            <li @if(Tools::routeIs('painel.neolabx.inovacao*')) class="active" @endif>
                <a href="{{ route('painel.neolabx.inovacao.index') }}">Inovação</a>
            </li>
            <li @if(Tools::routeIs('painel.neolabx.consultores*')) class="active" @endif>
                <a href="{{ route('painel.neolabx.consultores.index') }}">Rede de Consultores</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(Tools::routeIs(['painel.blog*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Blog <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.blog.categorias*')) class="active" @endif>
                <a href="{{ route('painel.blog.categorias.index') }}">Categorias</a>
            </li>
            <li @if(Tools::routeIs('painel.blog.artigos*')) class="active" @endif>
                <a href="{{ route('painel.blog.artigos.index') }}">Artigos</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(Tools::routeIs('painel.contato*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
                <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
            </li>
            <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.contato.recebidos.index') }}">
                    Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>

</ul>