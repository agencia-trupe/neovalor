@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>NEOLABX - Inovação |</small> Editar Inovação</h2>
</legend>

{!! Form::model($inovacao, [
'route' => ['painel.neolabx.inovacao.update', $inovacao->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.neolabx.inovacao.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection