@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        NEOLABX - Inovação
    </h2>
</legend>


@if(!count($inovacoes))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="neolabx_inovacao">
    <thead>
        <tr>
            <th>Imagem</th>
            <th>Título</th>
            <th>Subtítulo</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($inovacoes as $inovacao)
        <tr class="tr-row" id="{{ $inovacao->id }}">
            <td>
                <img src="{{ asset('assets/img/neolabx/'.$inovacao->imagem) }}" style="width: 100%; max-width:100px;" alt="">
            </td>
            <td>{{ $inovacao->titulo }}</td>
            <td>{{ $inovacao->subtitulo }}</td>
            <td class="crud-actions">
                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.neolabx.inovacao.edit', $inovacao->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection