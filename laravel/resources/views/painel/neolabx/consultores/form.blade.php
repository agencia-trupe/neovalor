@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('foto', 'Foto') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/neolabx/consultores/'.$consultor->foto) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('foto', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', 'E-mail (opcional)') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('whatsapp', 'Whatsapp (opcional)') !!}
    {!! Form::text('whatsapp', null, ['class' => 'form-control input-telefone']) !!}
    <p class="observacao">Inserir assim: <strong>11 90000 0000</strong></p>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('linkedin_nome', 'LinkedIn - Nome (opcional)') !!}
            {!! Form::text('linkedin_nome', null, ['class' => 'form-control']) !!}
            <p class="observacao">Inserir assim: <strong>linkedin.com/seu-nome</strong></p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('linkedin_link', 'LinkedIn (opcional)') !!}
            {!! Form::text('linkedin_link', null, ['class' => 'form-control']) !!}
            <p class="observacao">Inserir link completo</p>
        </div>
    </div>
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.neolabx.consultores.index') }}" class="btn btn-default btn-voltar">Voltar</a>