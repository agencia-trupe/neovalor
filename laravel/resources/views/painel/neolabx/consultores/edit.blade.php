@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>NEOLABX - Rede de Consultores |</small> Editar Consultor</h2>
</legend>

{!! Form::model($consultor, [
'route' => ['painel.neolabx.consultores.update', $consultor->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.neolabx.consultores.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection