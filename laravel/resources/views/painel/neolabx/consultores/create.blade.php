@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>NEOLABX - Rede de Consultores |</small> Adicionar Consultor</h2>
</legend>

{!! Form::open(['route' => 'painel.neolabx.consultores.store', 'files' => true]) !!}

@include('painel.neolabx.consultores.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection