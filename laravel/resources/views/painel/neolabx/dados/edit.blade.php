@extends('painel.common.template')

@section('content')

<legend>
    <h2>NEOLABX - Dados da página</h2>
</legend>

{!! Form::model($registro, [
'route' => ['painel.neolabx.update', $registro->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.neolabx.dados.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection