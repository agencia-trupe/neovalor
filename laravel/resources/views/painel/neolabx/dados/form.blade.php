@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título da página') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa (Imagem Inicial da Página)') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/neolabx/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('img_rede', 'Imagem (Rede de Consultores)') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/neolabx/'.$registro->img_rede) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('img_rede', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('frase_pt1', 'Frase (Parte 01)') !!}
    {!! Form::text('frase_pt1', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('frase_pt2', 'Frase (Parte 02)') !!}
    {!! Form::text('frase_pt2', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('item1', 'Item 01 (caixas azuis)') !!}
    {!! Form::text('item1', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('item2', 'Item 02 (caixas azuis)') !!}
    {!! Form::text('item2', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('item3', 'Item 03 (caixas azuis)') !!}
    {!! Form::text('item3', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_rede', 'Texto Rede de Consultores') !!}
    {!! Form::textarea('texto_rede', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}