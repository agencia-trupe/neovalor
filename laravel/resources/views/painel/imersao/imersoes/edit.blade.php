@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>EDUCAÇÃO CORPORATIVA - Imersões |</small> Editar Imersão</h2>
</legend>

{!! Form::model($imersao, [
'route' => ['painel.imersoes.update', $imersao->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.imersao.imersoes.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection