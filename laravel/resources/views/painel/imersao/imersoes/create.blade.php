@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>EDUCAÇÃO CORPORATIVA - Imersões |</small> Adicionar Imersão</h2>
</legend>

{!! Form::open(['route' => 'painel.imersoes.store', 'files' => true]) !!}

@include('painel.imersao.imersoes.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection