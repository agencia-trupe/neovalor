@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Ícone') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/imersoes/'.$imersao->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textItens']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.imersoes.index') }}" class="btn btn-default btn-voltar">Voltar</a>