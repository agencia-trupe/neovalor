@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>EDUCAÇÃO CORPORATIVA - Informações Extras |</small> Editar Informação</h2>
</legend>

{!! Form::model($info, [
'route' => ['painel.imersao.informacoes.update', $info->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.imersao.informacoes.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection