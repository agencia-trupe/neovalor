@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>EDUCAÇÃO CORPORATIVA - Informações Extras |</small> Adicionar Informação</h2>
</legend>

{!! Form::open(['route' => 'painel.imersao.informacoes.store', 'files' => true]) !!}

@include('painel.imersao.informacoes.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection