@extends('painel.common.template')

@section('content')

<legend>
    <h2>EDUCAÇÃO CORPORATIVA - Dados da página</h2>
</legend>

{!! Form::model($registro, [
'route' => ['painel.imersao.update', $registro->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.imersao.dados.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection