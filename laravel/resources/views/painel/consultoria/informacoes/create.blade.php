@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>CONSULTORIA - Informações Extras |</small> Adicionar Informação</h2>
</legend>

{!! Form::open(['route' => 'painel.consultoria.informacoes.store', 'files' => true]) !!}

@include('painel.consultoria.informacoes.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection