@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>CONSULTORIA - Informações Extras |</small> Editar Informação</h2>
</legend>

{!! Form::model($info, [
'route' => ['painel.consultoria.informacoes.update', $info->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.consultoria.informacoes.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection