@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>CONSULTORIA - Projetos |</small> Editar Projeto</h2>
</legend>

{!! Form::model($projeto, [
'route' => ['painel.projetos.update', $projeto->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.consultoria.projetos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection