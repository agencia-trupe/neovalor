@include('painel.common.flash')

<div class="well">
    <div class="checkbox" style="margin:0">
        <label style="font-weight:bold">
            {!! Form::hidden('novidade', 0) !!}
            {!! Form::checkbox('novidade') !!}
            Novidade
        </label>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('icone', 'Ícone') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/projetos/'.$projeto->icone) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('icone', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<p class="observacao" style="color:red; font-style:italic;"><strong>ATENÇÃO</strong>: para o correto funcionamento dos links entre EXPERTISES e PROJETOS, os <strong>TITULOS devem ser exatamente iguais</strong>.</p>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textItens']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.projetos.index') }}" class="btn btn-default btn-voltar">Voltar</a>