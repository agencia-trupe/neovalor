@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>CONSULTORIA - Projetos |</small> Adicionar Projeto</h2>
</legend>

{!! Form::open(['route' => 'painel.projetos.store', 'files' => true]) !!}

@include('painel.consultoria.projetos.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection