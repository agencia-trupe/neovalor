@extends('painel.common.template')

@section('content')

<legend>
    <h2>CONSULTORIA - Dados da página</h2>
</legend>

{!! Form::model($registro, [
'route' => ['painel.consultoria.update', $registro->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.consultoria.dados.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection