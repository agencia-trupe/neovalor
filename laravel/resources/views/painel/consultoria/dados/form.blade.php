@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('capa', 'Capa (Imagem Inicial da Página)') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/consultoria/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_consultoria', 'Texto Consultoria') !!}
    {!! Form::textarea('texto_consultoria', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_beneficios', 'Texto Benefícios') !!}
    {!! Form::textarea('texto_beneficios', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textItens']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}