@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<p class="observacao" style="color:red; font-style:italic;"><strong>ATENÇÃO</strong>: para o correto funcionamento dos links entre EXPERTISES e PROJETOS, os <strong>TITULOS devem ser exatamente iguais</strong>.</p>

<div class="form-group">
    {!! Form::label('frase', 'Frase') !!}
    {!! Form::text('frase', null, ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.expertises.index') }}" class="btn btn-default btn-voltar">Voltar</a>