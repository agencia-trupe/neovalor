@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>NEOVALOR - Expertises |</small> Editar Expertise</h2>
</legend>

{!! Form::model($expertise, [
'route' => ['painel.expertises.update', $expertise->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.empresa.expertises.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection