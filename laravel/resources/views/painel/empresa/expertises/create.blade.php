@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>NEOVALOR - Expertises |</small> Adicionar Expertise</h2>
</legend>

{!! Form::open(['route' => 'painel.expertises.store', 'files' => true]) !!}

@include('painel.empresa.expertises.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection