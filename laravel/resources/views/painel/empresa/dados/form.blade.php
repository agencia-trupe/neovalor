@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('capa', 'Capa (Imagem Inicial da Página)') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/empresa/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('sobre', 'Texto Sobre') !!}
    {!! Form::textarea('sobre', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('expertises', 'Texto Expertises') !!}
    {!! Form::textarea('expertises', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('metodologia', 'Texto Metodologia') !!}
    {!! Form::textarea('metodologia', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textItens']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem Metodologia') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/empresa/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('missao', 'Texto Missão') !!}
            {!! Form::textarea('missao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('visao', 'Texto Visão') !!}
            {!! Form::textarea('visao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('proposito', 'Texto Propósito') !!}
            {!! Form::textarea('proposito', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('valores', 'Texto Valores') !!}
            {!! Form::textarea('valores', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}