@extends('painel.common.template')

@section('content')

<legend>
    <h2>NEOVALOR - Dados da página</h2>
</legend>

{!! Form::model($registro, [
'route' => ['painel.empresa.update', $registro->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.empresa.dados.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection