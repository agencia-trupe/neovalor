@extends('painel.common.template')

@section('content')

<legend>
    <h2>NEOVALOR - Currículo</h2>
</legend>

{!! Form::model($registro, [
'route' => ['painel.curriculo.update', $registro->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.empresa.curriculo.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection