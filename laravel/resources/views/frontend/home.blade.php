@extends('frontend.common.template')

@section('content')

<section class="home">

    <div class="banners">
        @foreach($banners as $banner)
        @if($banner->link)
        <a href="{{ $banner->link }}" class="banner">
            <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" class="img-banner" alt="{{ $banner->titulo }}">
            <img src="{{ asset('assets/img/layout/marca-dagua-banners.svg') }}" alt="" class="overlay-marca">
            <div class="overlay-textos">
                <p class="titulo">{{ $banner->titulo }}</p>
                <p class="frase">{{ $banner->frase }}</p>
            </div>
        </a>
        @else
        <div class="banner">
            <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" class="img-banner" alt="{{ $banner->titulo }}">
            <img src="{{ asset('assets/img/layout/marca-dagua-banners.svg') }}" alt="" class="overlay-marca">
            <div class="overlay-textos">
                <p class="titulo">{{ $banner->titulo }}</p>
                <p class="frase">{{ $banner->frase }}</p>
            </div>
        </div>
        @endif
        @endforeach
        <div class=cycle-pager></div>
    </div>

    <div class="apresentacao">
        <a href="{{ route('neovalor') }}" class="centralizado">
            <div class="texto">{!! $home->texto_sobre !!}</div>
            <div class="foto">
                <img src="{{ asset('assets/img/empresa/'.$curriculo->foto) }}" class="img-foto" alt="{{ $curriculo->nome }}">
            </div>
        </a>
    </div>

    <div class="caixas-home">
        <a href="{{ $home->link_cx1 }}" class="caixa1">
            <div class="img-icone"></div>
            <p class="frase">{{ $home->frase_cx1 }}</p>
            <div class="img-seta"></div>
        </a>
        <a href="{{ $home->link_cx2 }}" class="caixa2">
            <div class="img-icone"></div>
            <p class="frase">{{ $home->frase_cx2 }}</p>
            <div class="img-seta"></div>
        </a>
        <a href="{{ $home->link_cx3 }}" class="caixa3">
            <div class="img-icone"></div>
            <p class="frase">{{ $home->frase_cx3 }}</p>
            <div class="img-seta"></div>
        </a>
    </div>

    <div class="clientes-e-projetos">
        <img src="{{ asset('assets/img/home/'.$home->imagem_bg) }}" class="bg-clientes-e-projetos" alt="">
        <div class="centralizado">
            <h2 class="titulo">CLIENTES E PROJETOS REALIZADOS</h2>
            <div class="texto">{!! $home->texto_projetos !!}</div>
            <a href="{{ route('clientes-e-projetos') }}" class="link-saber-mais">saber mais
                <!-- <img src="{{ asset('assets/img/layout/setinha-fina-branco.svg') }}" alt="" class="img-setinha"> -->
            </a>
            <div class="contadores" id="contadoresHome">
                <div class="contador">
                    <div class="quantidade" style="display: none;">{{ $contador->quantidade1 }}</div>
                    <div class="numero">0</div>
                    <p class="item">{{ $contador->item1 }}</p>
                </div>
                <div class="contador">
                    <div class="quantidade" style="display: none;">{{ $contador->quantidade2 }}</div>
                    <div class="numero">0</div>
                    <p class="item">{{ $contador->item2 }}</p>
                </div>
                <div class="contador">
                    <div class="quantidade" style="display: none;">{{ $contador->quantidade3 }}</div>
                    <div class="numero">0</div>
                    <p class="item">{{ $contador->item3 }}</p>
                </div>
                <div class="contador">
                    <div class="quantidade" style="display: none;">{{ $contador->quantidade4 }}</div>
                    <div class="numero">0</div>
                    <p class="item">{{ $contador->item4 }}</p>
                </div>
                <div class="contador">
                    <div class="quantidade" style="display: none;">{{ $contador->quantidade5 }}</div>
                    <div class="numero">0</div>
                    <p class="item">{{ $contador->item5 }}</p>
                </div>
                <div class="contador">
                    <div class="quantidade" style="display: none;">{{ $contador->quantidade6 }}</div>
                    <div class="numero">0</div>
                    <p class="item">{{ $contador->item6 }}</p>
                </div>
            </div>
        </div>
    </div>

    <div class="artigos">
        <h2 class="titulo">ARTIGOS</h2>
        @foreach($artigos as $key => $artigo)
        @if($key == 0)
        <a href="{{ route('blog.show', $artigo->slug) }}" class="artigo-destaque">
            <div class="capa">
                <img src="{{ asset('assets/img/blog/'.$artigo->capa) }}" class="img-capa" alt="">
            </div>
            <div class="dados">
                <div class="top">
                    <p class="ultimas">ÚLTIMAS DO MEU BLOG</p>
                    <div class="categoria-consultor">
                        @if($artigo->autor)
                        <p class="categoria">{{ $artigo->categoria_titulo }} |</p>
                        <p class="autor">{{ $artigo->autor }}</p>
                        @else
                        <p class="categoria">{{ $artigo->categoria_titulo }}</p>
                        @endif
                    </div>
                    <p class="titulo-artigo">{{ $artigo->titulo }}</p>
                </div>
                <p class="link-saber-mais">saber mais
                    <img src="{{ asset('assets/img/layout/setinha-fina-branco.svg') }}" alt="" class="img-setinha">
                </p>
            </div>
        </a>
        @else
        <a href="{{ route('blog.show', $artigo->slug) }}" class="artigo">
            <div class="capa">
                <img src="{{ asset('assets/img/blog/'.$artigo->capa) }}" class="img-capa" alt="">
            </div>
            <div class="dados">
                <div class="top">
                    <div class="categoria-consultor">
                        @if($artigo->autor)
                        <p class="categoria">{{ $artigo->categoria_titulo }} |</p>
                        <p class="autor">{{ $artigo->autor }}</p>
                        @else
                        <p class="categoria">{{ $artigo->categoria_titulo }}</p>
                        @endif
                    </div>
                    <p class="titulo-artigo">{{ $artigo->titulo }}</p>
                </div>
                <p class="link-saber-mais">saber mais
                    <img src="{{ asset('assets/img/layout/setinha-fina-branco.svg') }}" alt="" class="img-setinha">
                </p>
            </div>
        </a>
        @endif
        @endforeach
    </div>

</section>

@endsection