@extends('frontend.common.template')

@section('content')

<section class="blog-show">

    <div class="destaque">
        <div class="centralizado">
            <div class="categorias">
                @foreach($categorias as $categoria)
                <a href="{{ route('blog', ['categoria' => $categoria->slug]) }}" class="categoria {{ (isset($_GET['categoria']) && $_GET['categoria'] == $categoria->slug) ? 'active' : '' }}">
                    <img src="{{ asset('assets/img/layout/setinha-fina.svg') }}" alt="" class="img-setinha">
                    {{ $categoria->titulo }}
                </a>
                @endforeach
                <a href="{{ route('blog') }}" class="btn-voltar-blog"><img src="{{ asset('assets/img/layout/setinha-fina.svg') }}" alt="" class="img-voltar">voltar para os destaques</a>
            </div>
            <div class="artigo-destaque">
                <img src="{{ asset('assets/img/blog/'.$artigo->capa) }}" class="img-capa" alt="">
                <div class="overlay-destaque">
                    <h2 class="titulo">{{ $artigo->titulo }}</h2>
                    @if($artigo->autor)
                    <p class="categoria">{{ $artigo->categoria_titulo }} | <small>{{ $artigo->autor }}</small></p>
                    @else
                    <p class="categoria">{{ $artigo->categoria_titulo }}</p>
                    @endif
                    <p class="data">{{ strftime("%d %b %Y", strtotime($artigo->data)) }} · </p>
                    <img src="{{ asset('assets/img/layout/setinha-fina-branco.svg') }}" alt="" class="img-setinha">
                </div>
            </div>
        </div>
    </div>

    <div class="conteudo-artigo">
        <div class="texto">{!! $artigo->texto !!}</div>
        <div class="btn">
            <a href="{{ route('blog') }}" class="btn-voltar-blog"><img src="{{ asset('assets/img/layout/setinha-fina.svg') }}" alt="" class="img-voltar">voltar para os destaques</a>
        </div>
    </div>

</section>

@endsection