@extends('frontend.common.template')

@section('content')

<section class="neovalor">

    <div class="sobre-empresa">
        <div class="capa">
            <img src="{{ asset('assets/img/empresa/'.$empresa->capa) }}" class="img-capa" alt="">
        </div>
        <div class="sobre">
            <div class="texto">{!! $empresa->sobre !!}</div>
            <div class="foto">
                <img src="{{ asset('assets/img/empresa/'.$curriculo->foto) }}" class="img-foto" alt="">
            </div>
        </div>
    </div>
    <a href="{{ route('curriculo') }}" class="link-curriculo">curriculo
        <img src="{{ asset('assets/img/layout/setinha-fina.svg') }}" alt="" class="img-setinha">
    </a>

    <hr class="linha-neovalor">

    <div class="sobre-expertises">
        <div class="texto">{!! $empresa->expertises !!}</div>
        <p class="subtitulo">Expertise:</p>
        <div class="expertises">
            @foreach($expertises as $expertise)
            @php $expertise_slug = str_slug($expertise->titulo); @endphp
            <div class="expertise" id="{{$expertise_slug}}">
                <a href="{{ route('consultoria') . '/#' . $expertise_slug }}" class="link-expertise">
                    <p class="titulo">» {{ $expertise->titulo }}</p>
                    <p class="frase">{{ $expertise->frase }}</p>
                </a>
            </div>
            @endforeach
        </div>
    </div>

    <div class="minha-metodologia">
        <div class="capa">
            <img src="{{ asset('assets/img/empresa/'.$empresa->imagem) }}" class="img-metodologia" alt="">
        </div>
        <div class="sobre">
            <p class="subtitulo">Metodologia:</p>
            <div class="texto">{!! $empresa->metodologia !!}</div>
        </div>
    </div>

    <hr class="linha-neovalor">

    <div class="pilares">
        <div class="missao">
            <div class="img-missao">
                <img src="{{ asset('assets/img/layout/ico-missao.svg') }}" alt="">
            </div>
            <p class="titulo">Missão:</p>
            <div class="texto">{!! $empresa->missao !!}</div>
        </div>
        <div class="visao">
            <div class="img-visao">
                <img src="{{ asset('assets/img/layout/ico-visao.svg') }}" alt="">
            </div>
            <p class="titulo">Visão:</p>
            <div class="texto">{!! $empresa->visao !!}</div>
        </div>
        <div class="proposito">
            <div class="img-proposito">
                <img src="{{ asset('assets/img/layout/ico-proposito.svg') }}" alt="">
            </div>
            <p class="titulo">Propósito:</p>
            <div class="texto">{!! $empresa->proposito !!}</div>
        </div>
        <div class="valores">
            <div class="img-valores">
                <img src="{{ asset('assets/img/layout/ico-valores.svg') }}" alt="">
            </div>
            <p class="titulo">Valores:</p>
            <div class="texto">{!! $empresa->valores !!}</div>
        </div>
    </div>

</section>

@endsection