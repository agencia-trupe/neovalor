@extends('frontend.common.template')

@section('content')

<section class="blog">

    <div class="destaque">
        <div class="centralizado">
            <div class="categorias">
                @foreach($categorias as $categoria)
                <a href="{{ route('blog', ['categoria' => $categoria->slug]) }}" class="categoria {{ (isset($_GET['categoria']) && $_GET['categoria'] == $categoria->slug) ? 'active' : '' }}">
                    <img src="{{ asset('assets/img/layout/setinha-fina.svg') }}" alt="" class="img-setinha">
                    {{ $categoria->titulo }}
                </a>
                @endforeach
            </div>
            <a href="{{ route('blog.show', $destaque->slug) }}" class="artigo-destaque">
                <img src="{{ asset('assets/img/blog/'.$destaque->capa) }}" class="img-capa" alt="">
                <div class="overlay-destaque">
                    <h2 class="titulo">{{ $destaque->titulo }}</h2>
                    @if($destaque->autor)
                    <p class="categoria">{{ $destaque->categoria_titulo }} | <small>{{ $destaque->autor }}</small></p>
                    @else
                    <p class="categoria">{{ $destaque->categoria_titulo }}</p>
                    @endif
                    <p class="data">{{ strftime("%d %b %Y", strtotime($destaque->data)) }}</p>
                    <img src="{{ asset('assets/img/layout/setinha-fina-branco.svg') }}" alt="" class="img-setinha">
                </div>
            </a>
        </div>
    </div>

    <div class="artigos">
        @foreach($artigos as $artigo)
        <a href="{{ route('blog.show', $artigo->slug) }}" class="artigo">
            <img src="{{ asset('assets/img/blog/'.$artigo->capa) }}" class="img-capa" alt="">
            <h2 class="titulo">{{ $artigo->titulo }}</h2>
            @if($artigo->autor)
            <p class="categoria">{{ $artigo->categoria_titulo }} | <small>{{ $artigo->autor }}</small></p>
            @else
            <p class="categoria">{{ $artigo->categoria_titulo }}</p>
            @endif
            <p class="data">{{ strftime("%d %b %Y", strtotime($artigo->data)) }}</p>
            <img src="{{ asset('assets/img/layout/setinha-fina.svg') }}" alt="" class="img-setinha">
        </a>
        @endforeach
    </div>

    <button class="btn-mais-artigos">ver mais <img src="{{ asset('assets/img/layout/setinha-fina.svg') }}" alt="" class="img-setinha"></button>

</section>

@endsection