<header>
    <div class="centralizado">
        <a href="{{ route('home') }}" class="logo-header" title="{{ $config->title }}"><img src="{{ asset('assets/img/layout/marca-neovalor.svg') }}" alt="" class="img-logo"></a>
        <button id="mobile-toggle" type="button" role="button">
            <span class="lines internas"></span>
        </button>
        <nav>
            @include('frontend.common.nav')
        </nav>
    </div>

</header>