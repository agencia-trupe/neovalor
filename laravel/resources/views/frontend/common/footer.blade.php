<footer>
    <div class="centralizado">

        <div class="contatos">
            <a href="{{ route('home') }}" class="logo-footer" title="{{ $config->title }}"><img src="{{ asset('assets/img/layout/marca-neovalor-footer.svg') }}" alt="" class="img-logo-footer"></a>
            <div class="atendimentos">
                @php $celular = "+55".str_replace(" ", "", $contato->celular); @endphp
                <a href="https://api.whatsapp.com/send?phone={{ $celular }}" class="link-celular" target="_blank">{{ $contato->celular }}</a>
                <p class="horario">{{ $contato->atendimento }}</p>
                <div class="redes-sociais">
                    <!-- <a href="{{ $contato->facebook }}" target="_blank" class="facebook"></a> EXCLUIDO -->
                    <a href="{{ $contato->linkedin }}" target="_blank" class="linkedin"></a>
                </div>
            </div>
        </div>

        <div class="menus">
            <div class="parte-um">
                <a href="{{ route('neovalor') }}" @if(Tools::routeIs('neovalor*')) class="link-nav active" @endif class="link-nav">
                    <span class="linha">——</span>
                    <span class="nome">NEOVALOR</span>
                </a>
                <a href="{{ route('consultoria') }}" @if(Tools::routeIs('consultoria*')) class="link-nav active" @endif class="link-nav">
                    <span class="linha">——</span>
                    <span class="nome">CONSULTORIA</span>
                </a>
                <a href="{{ route('clientes-e-projetos') }}" @if(Tools::routeIs('clientes-e-projetos*')) class="link-nav active" @endif class="link-nav">
                    <span class="linha">——</span>
                    <span class="nome">CLIENTES E PROJETOS</span>
                </a>
            </div>
            <div class="parte-dois">
                <a href="{{ route('imersao') }}" @if(Tools::routeIs('imersao*')) class="link-nav active" @endif class="link-nav">
                    <span class="linha">——</span>
                    <span class="nome">EDUCAÇÃO CORPORATIVA</span>
                </a>
                <a href="{{ route('neolabx') }}" @if(Tools::routeIs('neolabx*')) class="link-nav active" @endif class="link-nav">
                    <span class="linha">——</span>
                    <span class="nome" style="font-weight: bold;">NeoLabX</span>
                </a>
                <a href="{{ route('blog') }}" @if(Tools::routeIs('blog*')) class="link-nav active" @endif class="link-nav">
                    <span class="linha">——</span>
                    <span class="nome">BLOG</span>
                </a>
                <a href="{{ route('contato') }}" @if(Tools::routeIs('contato*')) class="link-nav active" @endif class="link-nav">
                    <span class="linha">——</span>
                    <span class="nome">CONTATO</span>
                </a>
            </div>
        </div>

        <div class="informacoes">
            <a href="{{ route('politica-de-privacidade') }}" @if(Tools::routeIs('politica-de-privacidade*')) class="link-nav active" @endif class="link-nav">
                <span class="linha">——</span>
                <span class="nome">POLÍTICA DE PRIVACIDADE</span>
            </a>
            <p class="direitos">© {{ date('Y') }} {{ $config->title }} - Todos os direitos reservados</p>
            <div class="trupe">
                <a href="http://www.trupe.net" target="_blank" class="link-trupe">Criação de sites:</a>
                <a href="http://www.trupe.net" target="_blank" class="link-trupe">Trupe Agência Criativa</a>
            </div>
        </div>

    </div>
</footer>