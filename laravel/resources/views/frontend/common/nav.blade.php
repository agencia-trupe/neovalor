<div class="itens-menu">    
    <a href="{{ route('neovalor') }}" @if(Tools::routeIs(['neovalor*', 'curriculo*' ])) class="link-nav active" @endif class="link-nav">NEOVALOR</a>
    <a href="{{ route('consultoria') }}" @if(Tools::routeIs('consultoria*')) class="link-nav active" @endif class="link-nav">CONSULTORIA</a>
    <a href="{{ route('clientes-e-projetos') }}" @if(Tools::routeIs('clientes-e-projetos*')) class="link-nav active" @endif class="link-nav">CLIENTES E PROJETOS</a>
    <a href="{{ route('imersao') }}" @if(Tools::routeIs('imersao*')) class="link-nav active" @endif class="link-nav">EDUCAÇÃO CORPORATIVA</a>
    <a href="{{ route('neolabx') }}" @if(Tools::routeIs('neolabx*')) class="link-nav link-neolabx active" @endif class="link-nav link-neolabx">NeoLabX</a>
    <a href="{{ route('blog') }}" @if(Tools::routeIs('blog*')) class="link-nav active" @endif class="link-nav">BLOG</a>
    <a href="{{ route('contato') }}" @if(Tools::routeIs('contato*')) class="link-contato active" @endif class="link-contato">CONTATO</a>
    <div class="redes-sociais">
        <a href="{{ $contato->linkedin }}" target="_blank" class="linkedin"></a>
        <!-- <a href="{{ $contato->facebook }}" target="_blank" class="facebook"></a>  EXCLUIDO --> 
    </div>
</div>