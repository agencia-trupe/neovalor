@extends('frontend.common.template')

@section('content')

<section class="contato">
    <div class="centralizado">
        <div class="dados">
            @php $celular = "+55".str_replace(" ", "", $contato->celular); @endphp
            <a href="https://api.whatsapp.com/send?phone={{ $celular }}" class="whatsapp-atendimento" target="_blank">
                <img src="{{ asset('assets/img/layout/ico-whatsapp.svg') }}" alt="" class="img-wpp">
                <div class="textos">
                    <p class="telefone">{{ $contato->celular }}</p>
                    <p class="atendimento">{{ $contato->atendimento }}</p>
                </div>
            </a>

            <div class="form-infos">
                <form action="{{ route('contato.post') }}" method="POST" enctype="multipart/form-data" class="form-contato">
                    {!! csrf_field() !!}
                    <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                    <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                    <input type="text" name="telefone" placeholder="telefone" class="input-telefone" value="{{ old('telefone') }}">
                    <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
                    <button type="submit" class="btn-contato">ENVIAR</button>
                </form>
                <div class="infos">
                    <p class="tipo-trabalho">{{ $contato->tipo_trabalho }}</p>
                    <p class="endereco">{{ $contato->endereco }}</p>
                    <p class="bairro">{{ $contato->bairro }}</p>
                    <p class="cidade">{{ $contato->cidade }}</p>

                    @if($errors->any())
                    <div class="flash flash-erro">
                        @foreach($errors->all() as $error)
                        {!! $error !!}<br>
                        @endforeach
                    </div>
                    @endif

                    @if(session('enviado'))
                    <div class="flash flash-sucesso">
                        <p>Mensagem enviada com sucesso!</p>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>

@endsection