@extends('frontend.common.template')

@section('content')

<section class="neolabx-rede">

    <div class="dados-iniciais">
        <div class="imagens-iniciais">
            <img src="{{ asset('assets/img/neolabx/'.$dadosPagina->capa) }}" class="img-capa" alt="">
            <a href="" class="link-rede-consultores">
                <img src="{{ asset('assets/img/neolabx/'.$dadosPagina->img_rede) }}" class="img-rede" alt="">
            </a>
        </div>
        <div class="faixa-laranja">
            <div class="textos-faixa">
                <p class="pagina">NeoLabX</p>
                <p class="titulo">{{ $dadosPagina->titulo }}</p>
            </div>
        </div>
    </div>

    <div class="rede-consultores">
        <h2 class="titulo-rede">REDE DE CONSULTORES</h2>
        <div class="texto-rede">{!! $dadosPagina->texto_rede !!}</div>
        <a href="{{ route('neolabx') }}" class="link-voltar-topo">« VOLTAR PARA NEOLABX</a>
        @if(count($consultores) > 0)
        <div class="consultores masonry-grid">
            @foreach($consultores as $consultor)
            <div class="consultor">
                <img src="{{ asset('assets/img/neolabx/consultores/'.$consultor->foto) }}" class="foto-consultor" alt="">
                <div class="dados-consultor">
                    <p class="nome-consultor">{{ $consultor->nome }}</p>
                    <div class="texto-consultor">{!! $consultor->texto !!}</div>
                    @if($consultor->email)
                    <a href="mailto:{{ $consultor->email }}" class="email-consultor">
                        <img src="{{ asset('assets/img/layout/icone-emailconsultor.svg') }}" alt="" class="img-email">
                        {{ $consultor->email }}
                    </a>
                    @endif
                    @if($consultor->whatsapp)
                    @php $whatsapp = "+55".str_replace(" ", "", $consultor->whatsapp); @endphp
                    <a href="https://api.whatsapp.com/send?phone={{ $whatsapp }}" class="wpp-consultor" target="_blank">
                        <img src="{{ asset('assets/img/layout/icone-whatsappconsultor.svg') }}" alt="" class="img-wpp">
                        +55 {{ $consultor->whatsapp }}
                    </a>
                    @endif
                    @if($consultor->linkedin_link && $consultor->linkedin_nome)
                    <a href="{{ $consultor->linkedin_link }}" target="_blank" class="linkedin-consultor">
                        <img src="{{ asset('assets/img/layout/ico-linkedin-laranja.svg') }}" alt="" class="img-linkedin">
                        {{ $consultor->linkedin_nome }}
                    </a>
                    @endif
                </div>
            </div>
            @endforeach
        </div>
        <a href="{{ route('neolabx') }}" class="link-voltar-fim">
            <img src="{{ asset('assets/img/layout/setinha-fina.svg') }}" alt="" class="img-setinha">
            voltar
        </a>
        @endif
    </div>

</section>

@endsection