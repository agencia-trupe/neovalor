@extends('frontend.common.template')

@section('content')

<section class="imersao">

    <div class="apresentacao" style="background-image: url({{ asset('assets/img/imersao/'.$dados->capa) }})">
        <div class="centralizado">
            <h2 class="titulo">{{ $dados->titulo }}</h2>
            <div class="texto">{!! $dados->texto_imersao !!}</div>
            <div class="caixa-diferenciais">
                <p class="titulo">DIFERENCIAIS</p>
                <div class="texto-diferenciais">{!! $dados->texto_diferenciais !!}</div>
            </div>
        </div>
    </div>

    <div class="imersoes">
        @foreach($imersoes as $imersao)
        <a href="#" class="imersao">
            <div class="frente">
                <img src="{{ asset('assets/img/imersoes/'.$imersao->imagem) }}" alt="" class="img-imersao">
                <p class="titulo-imersao">{{ $imersao->titulo }}</p>
                <img src="{{ asset('assets/img/layout/setinha-fina.svg') }}" alt="" class="img-setinha">
            </div>
            <div class="verso">
                <div class="topo">
                    <img src="{{ asset('assets/img/imersoes/'.$imersao->imagem) }}" alt="" class="img-imersao">
                    <p class="titulo-imersao">{{ $imersao->titulo }}</p>
                </div>
                <div class="texto">{!! $imersao->texto !!}</div>
                <img src="{{ asset('assets/img/layout/setinha-fina-branco.svg') }}" alt="" class="img-setinha">
            </div>
        </a>
        @endforeach
    </div>

    <div class="informacoes-extras">
        @foreach($infosExtras as $info)
        <div class="informacao">
            <h2 class="titulo">{{ $info->titulo }}</h2>
            <div class="texto">{!! $info->texto !!}</div>
        </div>
        @endforeach
    </div>

</section>

@endsection