@extends('frontend.common.template')

@section('content')

<section class="consultoria">

    <div class="apresentacao">
        <img src="{{ asset('assets/img/consultoria/'.$dados->capa) }}" class="capa-consultoria" alt="">
        <div class="centralizado">
            <h2 class="titulo">{{ $dados->titulo }}</h2>
            <div class="texto">{!! $dados->texto_consultoria !!}</div>
            <div class="caixa-beneficios">
                <p class="titulo">BENEFÍCIOS</p>
                <div class="texto-beneficios">{!! $dados->texto_beneficios !!}</div>
            </div>
        </div>
    </div>

    <div class="projetos">
        <h2 class="titulo">PROJETOS</h2>
        <div class="itens-projetos">
            @foreach($projetos as $projeto)
            @php $projeto_slug = str_slug($projeto->titulo); @endphp
            <a href="#" class="projeto" id="{{$projeto_slug}}">
                <div class="frente">
                    <div class="icone">
                        <img src="{{ asset('assets/img/projetos/'.$projeto->icone) }}" alt="" class="img-icone">
                    </div>
                    <p class="titulo-projeto">{{ $projeto->titulo }}</p>
                    <img src="{{ asset('assets/img/layout/setinha-fina.svg') }}" alt="" class="img-setinha">
                </div>
                <div class="verso">
                    <p class="titulo-projeto">{{ $projeto->titulo }}</p>
                    <div class="texto">{!! $projeto->texto !!}</div>
                    <img src="{{ asset('assets/img/layout/setinha-fina-branco.svg') }}" alt="" class="img-setinha">
                </div>
            </a>
            @endforeach
            @foreach($projetosNovidades as $projeto)
            @php $projeto_slug = str_slug($projeto->titulo); @endphp
            <a href="#" class="projeto" id="{{$projeto_slug}}">
                <div class="frente novidade">
                    <div class="icone">
                        <img src="{{ asset('assets/img/projetos/'.$projeto->icone) }}" alt="" class="img-icone">
                    </div>
                    @if($projeto->novidade == 1)
                    <p class="titulo-novidade">NOVIDADE</p>
                    @endif
                    <p class="titulo-projeto">{{ $projeto->titulo }}</p>
                    <img src="{{ asset('assets/img/layout/setinha-fina.svg') }}" alt="" class="img-setinha">
                </div>
                <div class="verso">
                    <p class="titulo-projeto">{{ $projeto->titulo }}</p>
                    <div class="texto">{!! $projeto->texto !!}</div>
                    <img src="{{ asset('assets/img/layout/setinha-fina-branco.svg') }}" alt="" class="img-setinha">
                </div>
            </a>
            @endforeach
        </div>
    </div>

    <div class="informacoes-extras">
        @foreach($infosExtras as $info)
        <div class="informacao">
            <h2 class="titulo">{{ $info->titulo }}</h2>
            <div class="texto">{!! $info->texto !!}</div>
        </div>
        @endforeach
    </div>

</section>

@endsection