@extends('frontend.common.template')

@section('content')

<section class="clientes-e-projetos">

    <div class="parte-experiencias">
        <div class="capa" style="background-image: url({{ asset('assets/img/clientes-e-projetos/'.$dados->capa) }});"></div>
        <!-- <img src="{{ asset('assets/img/clientes-e-projetos/'.$dados->capa) }}" class="capa" alt=""> -->
        <div class="centralizado">
            <p class="frase-inicial">{{ $dados->frase }}</p>
            <div class="contadores" id="contadoresClientesProjetos">
                <div class="contador">
                    <div class="quantidade" style="display: none;">{{ $contador->quantidade1 }}</div>
                    <div class="numero">0</div>
                    <p class="item">{{ $contador->item1 }}</p>
                </div>
                <div class="contador">
                    <div class="quantidade" style="display: none;">{{ $contador->quantidade2 }}</div>
                    <div class="numero">0</div>
                    <p class="item">{{ $contador->item2 }}</p>
                </div>
                <div class="contador">
                    <div class="quantidade" style="display: none;">{{ $contador->quantidade3 }}</div>
                    <div class="numero">0</div>
                    <p class="item">{{ $contador->item3 }}</p>
                </div>
                <div class="contador">
                    <div class="quantidade" style="display: none;">{{ $contador->quantidade4 }}</div>
                    <div class="numero">0</div>
                    <p class="item">{{ $contador->item4 }}</p>
                </div>
                <div class="contador">
                    <div class="quantidade" style="display: none;">{{ $contador->quantidade5 }}</div>
                    <div class="numero">0</div>
                    <p class="item">{{ $contador->item5 }}</p>
                </div>
                <div class="contador">
                    <div class="quantidade" style="display: none;">{{ $contador->quantidade6 }}</div>
                    <div class="numero">0</div>
                    <p class="item">{{ $contador->item6 }}</p>
                </div>
            </div>

            <div class="grupos-experiencias">
                @foreach($grupos as $grupo)
                <div class="grupo" id="grupo-{{$grupo->id}}">
                    <div class="fechado">
                        <div class="icone"></div>
                        <h2 class="titulo-grupo">{{ $grupo->titulo }}</h2>
                        <span class="mais">+</span>
                    </div>
                    <div class="aberto">
                        <div class="icone"></div>
                        <div class="dados">
                            <h2 class="titulo-grupo">{{ $grupo->titulo }}</h2>
                            <a href="" class="fechar">X</a>
                            <div class="experiencias">
                                @foreach($experiencias as $experiencia)
                                @if($experiencia->grupo_id == $grupo->id)
                                <div class="experiencia">
                                    <p class="periodo">{{ $experiencia->periodo }}</p>
                                    <h4 class="titulo">{{ $experiencia->titulo }}</h4>
                                    <div class="texto">{!! $experiencia->texto !!}</div>
                                </div>
                                @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="parte-treinamentos">
        <h2 class="titulo">TREINAMENTOS</h2>
        <div class="texto-treinamentos">{!! $dados->texto_treinamentos !!}</div>
        <div class="treinamentos">
            @foreach($treinamentos as $treinamento)
            <div class="treinamento">
                <div class="img-treinamento">
                    <img src="{{ asset('assets/img/treinamentos/'.$treinamento->imagem) }}" class="logo" alt="">
                </div>
                <div class="dados-treinamento">
                    <p class="empresa">{{ $treinamento->empresa }}</p>
                    <p class="ano-tipo">{{ $treinamento->ano }} : {{ $treinamento->tipo }}</p>
                    <p class="titulo-treinamento">{{ $treinamento->titulo }}</p>
                    <p class="consultor">Consultor: {{ $treinamento->consultor }}</p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

@endsection