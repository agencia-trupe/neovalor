@extends('frontend.common.template')

@section('content')

<section class="neolabx">

    <div class="dados-iniciais">
        <div class="imagens-iniciais">
            <img src="{{ asset('assets/img/neolabx/'.$dadosPagina->capa) }}" class="img-capa" alt="">
            <a href="{{ route('neolabx.rede') }}" class="link-rede-consultores">
                <img src="{{ asset('assets/img/neolabx/'.$dadosPagina->img_rede) }}" class="img-rede" alt="">
                <p class="titulo-rede">REDE DE CONSULTORES »</p>
            </a>
        </div>
        <div class="frases-e-itens">
            <div class="frases">
                <p class="frase1">{{ $dadosPagina->frase_pt1 }}</p>
                <p class="frase2">{{ $dadosPagina->frase_pt2 }}</p>
            </div>
            <div class="itens-neolabx">
                <p class="item1">{{ $dadosPagina->item1 }}</p>
                <p class="item2">{{ $dadosPagina->item2 }}</p>
                <p class="item3">{{ $dadosPagina->item3 }}</p>
            </div>
        </div>
        <div class="faixa-laranja">
            <div class="textos-faixa">
                <p class="pagina">NeoLabX</p>
                <p class="titulo">{{ $dadosPagina->titulo }}</p>
            </div>
        </div>
    </div>

    <div class="inovacao">
        <h2 class="titulo-inovacao">INOVAÇÃO</h2>
        <div class="centralizado">
            @foreach($inovacoes as $inovacao)
            <a href="#" class="link-inovacao">
                <div class="frente">
                    <img src="{{ asset('assets/img/neolabx/'.$inovacao->imagem) }}" alt="{{ $inovacao->titulo }}" class="img-inovacao">
                    <p class="titulo-frente">{{ $inovacao->titulo }}</p>
                    <p class="subtitulo-frente">{{ $inovacao->subtitulo }}</p>
                    <img src="{{ asset('assets/img/layout/setinha-fina-branco.svg') }}" alt="" class="img-setinha">
                </div>
                <div class="verso">
                    <p class="titulo-verso">{{ $inovacao->titulo }}</p>
                    <p class="subtitulo-verso">{{ $inovacao->subtitulo }}</p>
                    <div class="texto-verso">{!! $inovacao->texto !!}</div>
                    <div class="setinha-verso">
                        <img src="{{ asset('assets/img/layout/setinha-fina-branco.svg') }}" alt="" class="img-setinha">
                    </div>
                </div>
            </a>
            @endforeach
        </div>
        <div class="contatos">
            <p class="frase-contato">VAMOS MELHORAR JUNTOS, LIGUE:</p>
            @php $celular = "+55".str_replace(" ", "", $contato->celular); @endphp
            <a href="https://api.whatsapp.com/send?phone={{ $celular }}" class="link-whatsapp" target="_blank">
                <img src="{{ asset('assets/img/layout/ico-whatsapp2.svg') }}" alt="" class="img-whats">
                <p class="telefone">{{ $contato->celular }}</p>
            </a>
            <a href="mailto:{{ $contato->email }}" target="_blank" class="link-email">
                <div class="icone-email">
                    <img src="{{ asset('assets/img/layout/ico-email.svg') }}" alt="" class="img-email">
                </div>
                <p class="email">{{ $contato->email }}</p>
            </a>
        </div>
    </div>

    <div class="blogx">
        @if(isset($artigoNeolabx))
        <h2 class="titulo-blogx">BlogX</h2>
        <a href="{{ route('blog.show', $artigoNeolabx->slug) }}" class="artigo-blogx">
            <div class="capa">
                <img src="{{ asset('assets/img/blog/'.$artigoNeolabx->capa) }}" class="img-capa" alt="">
            </div>
            <div class="dados">
                <div class="top">
                    <p class="subtitulo">ARTIGO BLOGX</p>
                    <div class="categoria-consultor">
                        @if($artigoNeolabx->autor)
                        <p class="categoria">{{ $artigoNeolabx->categoria_titulo }} |</p>
                        <p class="autor">{{ $artigoNeolabx->autor }}</p>
                        @else
                        <p class="categoria">{{ $artigoNeolabx->categoria_titulo }}</p>
                        @endif
                    </div>
                    <p class="titulo-artigo">{{ $artigoNeolabx->titulo }}</p>
                </div>
                <p class="link-saber-mais">saber mais
                    <img src="{{ asset('assets/img/layout/setinha-fina-branco.svg') }}" alt="" class="img-setinha">
                </p>
            </div>
        </a>
        @endif
    </div>

</section>

@endsection