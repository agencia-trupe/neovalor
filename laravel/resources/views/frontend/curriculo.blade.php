@extends('frontend.common.template')

@section('content')

<section class="curriculo">

    <div class="foto">
        <img src="{{ asset('assets/img/empresa/'.$curriculo->foto) }}" class="img-foto" alt="">
    </div>

    <div class="dados">
        <p class="nome">{{ $curriculo->nome }}</p>
        <p class="titulo">{{ $curriculo->titulo }}</p>
        <div class="texto">{!! $curriculo->texto !!}</div>
        <a href="{{ route('neovalor') }}" class="link-voltar">
            <img src="{{ asset('assets/img/layout/setinha-fina.svg') }}" alt="" class="img-setinha">
            voltar
        </a>
    </div>

</section>

@endsection