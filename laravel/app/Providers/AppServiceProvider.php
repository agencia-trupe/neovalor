<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*', function ($view) {
            $view->with('config', \App\Models\Configuracao::first());
        });

        view()->composer('frontend.*', function ($view) {
            $view->with('curriculo', \App\Models\Curriculo::first());
            $view->with('contato', \App\Models\Contato::first());
        });

        view()->composer('painel.common.*', function ($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
        });
    }

    public function register()
    {
        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');
    }
}
