<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Http\Controllers';

    public function boot(Router $router)
    {
        $router->model('neolabx_consultores', 'App\Models\NeolabxConsultor');
        $router->model('neolabx_inovacao', 'App\Models\NeolabxInovacao');
        $router->model('neolabx', 'App\Models\Neolabx');
        $router->model('aceite_de_cookies', 'App\Models\AceiteDeCookies');
        $router->model('blog_artigos', 'App\Models\BlogArtigo');
        $router->model('blog_categorias', 'App\Models\BlogCategoria');
        $router->model('imersao_informacoes', 'App\Models\ImersaoInformacao');
        $router->model('imersoes', 'App\Models\Imersao');
        $router->model('imersao_dados', 'App\Models\ImersaoDados');
        $router->model('treinamentos', 'App\Models\Treinamento');
        $router->model('experiencias', 'App\Models\Experiencia');
        $router->model('grupos_experiencias', 'App\Models\GrupoExperiencias');
        $router->model('contadores_projetos', 'App\Models\ContadorProjetos');
        $router->model('clientes_projetos', 'App\Models\ClienteProjeto');
        $router->model('consultoria_informacoes', 'App\Models\ConsultoriaInformacao');
        $router->model('projetos', 'App\Models\Projeto');
        $router->model('consultoria', 'App\Models\Consultoria');
        $router->model('curriculo', 'App\Models\Curriculo');
        $router->model('expertises', 'App\Models\Expertise');
        $router->model('empresa', 'App\Models\Empresa');
        $router->model('contadores_home', 'App\Models\ContadorHome');
        $router->model('home', 'App\Models\Home');
        $router->model('banners', 'App\Models\Banner');
        $router->model('contatos_recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('politica-de-privacidade', 'App\Models\PoliticaDePrivacidade');
        $router->model('configuracoes', 'App\Models\Configuracao');
        $router->model('usuarios', 'App\Models\User');
        
        parent::boot($router);
    }

    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
