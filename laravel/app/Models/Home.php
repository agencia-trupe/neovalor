<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    protected $table = 'home';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem_bg', [
            'width'  => 1980,
            'height' => null,
            'path'   => 'assets/img/home/'
        ]);
    }
}
