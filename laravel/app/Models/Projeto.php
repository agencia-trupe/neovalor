<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class Projeto extends Model
{
    protected $table = 'projetos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeNovidades($query)
    {
        return $query->where('novidade', '=', 1);
    }

    public static function upload_icone()
    {
        return CropImage::make('icone', [
            'width'  => null,
            'height' => null,
            'transparent' => true,
            'path'   => 'assets/img/projetos/'
        ]);
    }
}
