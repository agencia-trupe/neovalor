<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GrupoExperiencias extends Model
{
    protected $table = 'grupos_experiencias';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function experiencias()
    {
        return $this->hasMany('App\Models\Experiencia', 'grupo_id')->ordenados();
    }
}
