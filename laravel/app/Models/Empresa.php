<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = 'empresa';

    protected $guarded = ['id'];

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 800,
            'height' => null,
            'path'   => 'assets/img/empresa/'
        ]);
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 800,
            'height' => null,
            'path'   => 'assets/img/empresa/'
        ]);
    }
}
