<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContadorHome extends Model
{
    protected $table = 'contadores_home';

    protected $guarded = ['id'];
}
