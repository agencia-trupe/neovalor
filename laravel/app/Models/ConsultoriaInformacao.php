<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConsultoriaInformacao extends Model
{
    protected $table = 'consultoria_informacoes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
