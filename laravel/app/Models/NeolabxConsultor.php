<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class NeolabxConsultor extends Model
{
    protected $table = 'neolabx_consultores';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_foto()
    {
        return CropImage::make('foto', [
            'width'  => 200,
            'height' => null,
            'path'   => 'assets/img/neolabx/consultores/'
        ]);
    }
}
