<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class ClienteProjeto extends Model
{
    protected $table = 'clientes_projetos';

    protected $guarded = ['id'];

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 1980,
            'height' => null,
            'path'   => 'assets/img/clientes-e-projetos/'
        ]);
    }
}
