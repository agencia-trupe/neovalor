<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContadorProjetos extends Model
{
    protected $table = 'contadores_projetos';

    protected $guarded = ['id'];
}
