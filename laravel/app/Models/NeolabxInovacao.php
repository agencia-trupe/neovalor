<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class NeolabxInovacao extends Model
{
    protected $table = 'neolabx_inovacao';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 350,
            'height' => 350,
            'path'   => 'assets/img/neolabx/'
        ]);
    }
}
