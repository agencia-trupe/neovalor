<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class Neolabx extends Model
{
    protected $table = 'neolabx';

    protected $guarded = ['id'];

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 810,
            'height' => null,
            'path'   => 'assets/img/neolabx/'
        ]);
    }

    public static function upload_img_rede()
    {
        return CropImage::make('img_rede', [
            'width'  => 457,
            'height' => null,
            'path'   => 'assets/img/neolabx/'
        ]);
    }
}
