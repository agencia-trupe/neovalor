<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogCategoria extends Model
{
    protected $table = 'blog_categorias';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function artigos()
    {
        return $this->hasMany('App\Models\BlogArtigo', 'categoria_id')->ordenados();
    }
}
