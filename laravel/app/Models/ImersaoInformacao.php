<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImersaoInformacao extends Model
{
    protected $table = 'imersao_informacoes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
