<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class Curriculo extends Model
{
    protected $table = 'curriculo';

    protected $guarded = ['id'];

    public static function upload_foto()
    {
        return CropImage::make('foto', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/empresa/'
        ]);
    }
}
