<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class BlogArtigo extends Model
{
    protected $table = 'blog_artigos';

    protected $guarded = ['id'];

    public function getCreatedAtAttribute($data)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $data)->format('d/m/Y');
    }

    public function scopeCategoria($query, $id)
    {
        return $query->where('categoria_id', $id);
    }

    public function scopeNeolabx($query)
    {
        return $query->where('neolabx', '=', 1);
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 600,
            'height' => null,
            'path'   => 'assets/img/blog/'
        ]);
    }
}
