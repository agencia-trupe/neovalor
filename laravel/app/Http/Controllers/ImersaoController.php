<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Imersao;
use App\Models\ImersaoDados;
use App\Models\ImersaoInformacao;

class ImersaoController extends Controller
{
    public function index()
    {
        $dados = ImersaoDados::first();
        $imersoes = Imersao::ordenados()->get();
        $infosExtras = ImersaoInformacao::ordenados()->get();

        return view('frontend.imersao', compact('dados', 'imersoes', 'infosExtras'));
    }
}
