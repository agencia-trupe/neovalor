<?php

namespace App\Http\Controllers;

use App\Models\BlogArtigo;
use App\Models\Neolabx;
use App\Models\NeolabxConsultor;
use App\Models\NeolabxInovacao;

class NeolabxController extends Controller
{
    public function index()
    {
        $dadosPagina = Neolabx::first();
        $inovacoes = NeolabxInovacao::orderBy('id', 'asc')->get();
        $artigoNeolabx = BlogArtigo::neolabx()
            ->join('blog_categorias', 'blog_categorias.id', '=', 'blog_artigos.categoria_id')
            ->select('blog_categorias.titulo as categoria_titulo', 'blog_artigos.*')
            ->orderBy('data', 'desc')->first();

        return view('frontend.neolabx', compact('dadosPagina', 'inovacoes', 'artigoNeolabx'));
    }

    public function indexRede()
    {
        $dadosPagina = Neolabx::first();
        $consultores = NeolabxConsultor::ordenados()->get();

        return view('frontend.neolabx-rede', compact('dadosPagina', 'consultores'));
    }
}
