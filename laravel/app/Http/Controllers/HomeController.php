<?php

namespace App\Http\Controllers;

use App\Models\AceiteDeCookies;
use App\Models\Banner;
use App\Models\BlogArtigo;
use App\Models\ContadorHome;
use App\Models\Home;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $banners = Banner::ordenados()->get();
        $home = Home::first();
        $contador = ContadorHome::first();
        $artigos = BlogArtigo::join('blog_categorias', 'blog_categorias.id', '=', 'blog_artigos.categoria_id')
            ->select('blog_categorias.id as categoria_id', 'blog_categorias.titulo as categoria_titulo', 'blog_artigos.id', 'blog_artigos.capa', 'blog_artigos.slug', 'blog_artigos.titulo', 'blog_artigos.autor', 'blog_artigos.data')
            ->orderBy('data', 'desc')->take(4)->get();

        $verificacao = AceiteDeCookies::where('ip', $request->ip())->first();

        return view('frontend.home', compact('banners', 'home', 'contador', 'artigos', 'verificacao'));
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect('/');
    }
}
