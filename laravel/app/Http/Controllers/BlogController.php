<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\BlogArtigo;
use App\Models\BlogCategoria;

class BlogController extends Controller
{
    public function index()
    {
        $categorias = BlogCategoria::ordenados()->get();

        if (isset($_GET['categoria'])) {

            $categoria = $_GET['categoria'];
            $destaque = BlogArtigo::join('blog_categorias', 'blog_categorias.id', '=', 'blog_artigos.categoria_id')
                ->select('blog_categorias.id as categoria_id', 'blog_categorias.titulo as categoria_titulo', 'blog_categorias.slug as categoria_slug', 'blog_artigos.id', 'blog_artigos.capa', 'blog_artigos.slug', 'blog_artigos.titulo', 'blog_artigos.autor', 'blog_artigos.data')
                ->where('blog_categorias.slug', $categoria)
                ->orderBy('data', 'desc')->first();
            $artigos = BlogArtigo::join('blog_categorias', 'blog_categorias.id', '=', 'blog_artigos.categoria_id')
                ->select('blog_categorias.id as categoria_id', 'blog_categorias.titulo as categoria_titulo', 'blog_categorias.slug as categoria_slug', 'blog_artigos.id', 'blog_artigos.capa', 'blog_artigos.slug', 'blog_artigos.titulo', 'blog_artigos.autor', 'blog_artigos.data')
                ->where('blog_categorias.slug', $categoria)
                ->orderBy('blog_artigos.data', 'desc')->skip(1)->take(PHP_INT_MAX)->get();
            
        } else {

            $destaque = BlogArtigo::join('blog_categorias', 'blog_categorias.id', '=', 'blog_artigos.categoria_id')
                ->select('blog_categorias.id as categoria_id', 'blog_categorias.titulo as categoria_titulo', 'blog_categorias.slug as categoria_slug', 'blog_artigos.id', 'blog_artigos.capa', 'blog_artigos.slug', 'blog_artigos.titulo', 'blog_artigos.autor', 'blog_artigos.data')
                ->orderBy('data', 'desc')->first();
            $artigos = BlogArtigo::join('blog_categorias', 'blog_categorias.id', '=', 'blog_artigos.categoria_id')
                ->select('blog_categorias.id as categoria_id', 'blog_categorias.titulo as categoria_titulo', 'blog_categorias.slug as categoria_slug', 'blog_artigos.id', 'blog_artigos.capa', 'blog_artigos.slug', 'blog_artigos.titulo', 'blog_artigos.autor', 'blog_artigos.data')
                ->orderBy('blog_artigos.data', 'desc')->skip(1)->take(PHP_INT_MAX)->get();
        }

        return view('frontend.blog', compact('categorias', 'destaque', 'artigos'));
    }

    public function show($slug)
    {
        $categorias = BlogCategoria::ordenados()->get();

        $artigo = BlogArtigo::join('blog_categorias', 'blog_categorias.id', '=', 'blog_artigos.categoria_id')
            ->select('blog_categorias.id as categoria_id', 'blog_categorias.titulo as categoria_titulo', 'blog_categorias.slug as categoria_slug', 'blog_artigos.id', 'blog_artigos.capa', 'blog_artigos.slug', 'blog_artigos.titulo', 'blog_artigos.autor', 'blog_artigos.data', 'blog_artigos.texto')
            ->where('blog_artigos.slug', $slug)->first();

        return view('frontend.blog-show', compact('categorias', 'artigo'));
    }
}
