<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\ClienteProjeto;
use App\Models\ContadorProjetos;
use App\Models\Experiencia;
use App\Models\GrupoExperiencias;
use App\Models\Treinamento;

class ClientesProjetosController extends Controller
{
    public function index()
    {
        $dados = ClienteProjeto::first();
        $contador = ContadorProjetos::first();

        $grupos = GrupoExperiencias::ordenados()->get();
        $experiencias = Experiencia::ordenados()->get();

        $treinamentos = Treinamento::ordenados()->get();

        return view('frontend.clientes-e-projetos', compact('dados', 'contador', 'grupos', 'experiencias', 'treinamentos'));
    }
}
