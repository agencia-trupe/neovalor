<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Curriculo;
use App\Models\Empresa;
use App\Models\Expertise;

class NeovalorController extends Controller
{
    public function index()
    {
        $empresa = Empresa::first();
        $expertises = Expertise::ordenados()->get();

        return view('frontend.neovalor', compact('empresa', 'expertises'));
    }

    public function indexCurriculo()
    {
        $curriculo = Curriculo::first();

        return view('frontend.curriculo', compact('curriculo'));
    }
}
