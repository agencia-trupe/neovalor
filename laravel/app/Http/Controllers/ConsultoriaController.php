<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Consultoria;
use App\Models\ConsultoriaInformacao;
use App\Models\Projeto;

class ConsultoriaController extends Controller
{
    public function index()
    {
        $dados = Consultoria::first();
        $projetos = Projeto::where('novidade', 0)->ordenados()->get();
        $projetosNovidades = Projeto::novidades()->ordenados()->get();
        $infosExtras = ConsultoriaInformacao::ordenados()->get();

        return view('frontend.consultoria', compact('dados', 'projetos', 'projetosNovidades', 'infosExtras'));
    }
}
