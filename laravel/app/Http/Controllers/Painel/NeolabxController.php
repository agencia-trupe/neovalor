<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\NeolabxRequest;
use App\Models\Neolabx;

class NeolabxController extends Controller
{
    public function index()
    {
        $registro = Neolabx::first();

        return view('painel.neolabx.dados.edit', compact('registro'));
    }

    public function update(NeolabxRequest $request, Neolabx $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Neolabx::upload_capa();
            if (isset($input['img_rede'])) $input['img_rede'] = Neolabx::upload_img_rede();

            $registro->update($input);

            return redirect()->route('painel.neolabx.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
