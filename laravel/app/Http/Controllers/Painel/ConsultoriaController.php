<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ConsultoriaRequest;
use App\Models\Consultoria;

class ConsultoriaController extends Controller
{
    public function index()
    {
        $registro = Consultoria::first();

        return view('painel.consultoria.dados.edit', compact('registro'));
    }

    public function update(ConsultoriaRequest $request, Consultoria $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Consultoria::upload_capa();

            $registro->update($input);

            return redirect()->route('painel.consultoria.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
