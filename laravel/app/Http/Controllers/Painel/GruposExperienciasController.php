<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\GruposExperienciasRequest;
use App\Models\GrupoExperiencias;

class GruposExperienciasController extends Controller
{
    public function index()
    {
        $grupos = GrupoExperiencias::ordenados()->get();

        return view('painel.clientes-e-projetos.grupos.index', compact('grupos'));
    }

    public function create()
    {
        return view('painel.clientes-e-projetos.grupos.create');
    }

    public function store(GruposExperienciasRequest $request)
    {
        try {
            $input = $request->all();

            GrupoExperiencias::create($input);

            return redirect()->route('painel.grupos-experiencias.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(GrupoExperiencias $grupo)
    {
        return view('painel.clientes-e-projetos.grupos.edit', compact('grupo'));
    }

    public function update(GruposExperienciasRequest $request, GrupoExperiencias $grupo)
    {
        try {
            $input = $request->all();

            $grupo->update($input);

            return redirect()->route('painel.grupos-experiencias.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(GrupoExperiencias $grupo)
    {
        try {
            $grupo->delete();

            return redirect()->route('painel.grupos-experiencias.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
