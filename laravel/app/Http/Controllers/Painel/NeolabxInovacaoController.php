<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\NeolabxInovacaoRequest;
use App\Models\NeolabxInovacao;

class NeolabxInovacaoController extends Controller
{
    public function index()
    {
        $inovacoes = NeolabxInovacao::orderBy('id', 'asc')->get();

        return view('painel.neolabx.inovacao.index', compact('inovacoes'));
    }

    public function edit(NeolabxInovacao $inovacao)
    {
        return view('painel.neolabx.inovacao.edit', compact('inovacao'));
    }

    public function update(NeolabxInovacaoRequest $request, NeolabxInovacao $inovacao)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, "-");

            if (isset($input['imagem'])) $input['imagem'] = NeolabxInovacao::upload_imagem();

            $inovacao->update($input);

            return redirect()->route('painel.neolabx.inovacao.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
