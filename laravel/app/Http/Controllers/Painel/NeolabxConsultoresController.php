<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\NeolabxConsultoresRequest;
use App\Models\NeolabxConsultor;

class NeolabxConsultoresController extends Controller
{
    public function index()
    {
        $consultores = NeolabxConsultor::ordenados()->get();

        return view('painel.neolabx.consultores.index', compact('consultores'));
    }

    public function create()
    {
        return view('painel.neolabx.consultores.create');
    }

    public function store(NeolabxConsultoresRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['foto'])) $input['foto'] = NeolabxConsultor::upload_foto();

            NeolabxConsultor::create($input);

            return redirect()->route('painel.neolabx.consultores.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit($consultor_id)
    {
        $consultor = NeolabxConsultor::where('id', $consultor_id)->first();

        return view('painel.neolabx.consultores.edit', compact('consultor'));
    }

    public function update(NeolabxConsultoresRequest $request, $consultor_id)
    {
        try {
            $consultor = NeolabxConsultor::where('id', $consultor_id)->first();

            $input = $request->all();

            if (isset($input['foto'])) $input['foto'] = NeolabxConsultor::upload_foto();

            $consultor->update($input);

            return redirect()->route('painel.neolabx.consultores.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy($consultor_id)
    {
        try {
            $consultor = NeolabxConsultor::where('id', $consultor_id)->first();
            $consultor->delete();

            return redirect()->route('painel.neolabx.consultores.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
