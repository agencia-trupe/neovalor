<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ConsultoriaInformacoesRequest;
use App\Models\ConsultoriaInformacao;

class ConsultoriaInformacoesController extends Controller
{
    public function index()
    {
        $infos = ConsultoriaInformacao::ordenados()->get();

        return view('painel.consultoria.informacoes.index', compact('infos'));
    }

    public function create()
    {
        return view('painel.consultoria.informacoes.create');
    }

    public function store(ConsultoriaInformacoesRequest $request)
    {
        try {
            $input = $request->all();

            ConsultoriaInformacao::create($input);

            return redirect()->route('painel.consultoria.informacoes.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit($info_id)
    {
        $info = ConsultoriaInformacao::where('id', $info_id)->first();
        
        return view('painel.consultoria.informacoes.edit', compact('info'));
    }

    public function update(ConsultoriaInformacoesRequest $request, $info_id)
    {
        try {
            $input = $request->all();

            $info = ConsultoriaInformacao::where('id', $info_id)->first();

            $info->update($input);

            return redirect()->route('painel.consultoria.informacoes.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy($info_id)
    {
        try {
            $info = ConsultoriaInformacao::where('id', $info_id)->first();
            
            $info->delete();

            return redirect()->route('painel.consultoria.informacoes.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
