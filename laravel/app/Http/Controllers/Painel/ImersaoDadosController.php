<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ImersaoDadosRequest;
use App\Models\ImersaoDados;

class ImersaoDadosController extends Controller
{
    public function index()
    {
        $registro = ImersaoDados::first();

        return view('painel.imersao.dados.edit', compact('registro'));
    }

    public function update(ImersaoDadosRequest $request, $registro_id)
    {
        try {
            $input = $request->all();

            $registro = ImersaoDados::where('id', $registro_id)->first();

            if (isset($input['capa'])) $input['capa'] = ImersaoDados::upload_capa();

            $registro->update($input);

            return redirect()->route('painel.imersao.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
