<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClientesProjetosRequest;
use App\Models\ClienteProjeto;

class ClientesProjetosController extends Controller
{
    public function index()
    {
        $registro = ClienteProjeto::first();

        return view('painel.clientes-e-projetos.dados.edit', compact('registro'));
    }

    public function update(ClientesProjetosRequest $request, $registro_id)
    {
        try {
            $input = $request->all();

            $registro = ClienteProjeto::where('id', $registro_id)->first();

            if (isset($input['capa'])) $input['capa'] = ClienteProjeto::upload_capa();

            $registro->update($input);

            return redirect()->route('painel.clientes-e-projetos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
