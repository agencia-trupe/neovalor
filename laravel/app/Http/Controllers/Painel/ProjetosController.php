<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProjetosRequest;
use App\Models\Projeto;

class ProjetosController extends Controller
{
    public function index()
    {
        if (isset($_GET['novidades'])) {
            $linha = $_GET['novidades'];
            $projetos = Projeto::novidades()->ordenados()->get();
        } else {
            $projetos = Projeto::ordenados()->get();
        }

        return view('painel.consultoria.projetos.index', compact('projetos'));
    }

    public function create()
    {
        return view('painel.consultoria.projetos.create');
    }

    public function store(ProjetosRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['icone'])) $input['icone'] = Projeto::upload_icone();

            Projeto::create($input);

            return redirect()->route('painel.projetos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Projeto $projeto)
    {
        return view('painel.consultoria.projetos.edit', compact('projeto'));
    }

    public function update(ProjetosRequest $request, Projeto $projeto)
    {
        try {
            $input = $request->all();

            if (isset($input['icone'])) $input['icone'] = Projeto::upload_icone();

            $projeto->update($input);

            return redirect()->route('painel.projetos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Projeto $projeto)
    {
        try {
            $projeto->delete();

            return redirect()->route('painel.projetos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
