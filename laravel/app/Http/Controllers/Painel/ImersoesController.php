<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ImersoesRequest;
use App\Models\Imersao;

class ImersoesController extends Controller
{
    public function index()
    {
        $imersoes = Imersao::ordenados()->get();

        return view('painel.imersao.imersoes.index', compact('imersoes'));
    }

    public function create()
    {
        return view('painel.imersao.imersoes.create');
    }

    public function store(ImersoesRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Imersao::upload_imagem();

            Imersao::create($input);

            return redirect()->route('painel.imersoes.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Imersao $imersao)
    {
        return view('painel.imersao.imersoes.edit', compact('imersao'));
    }

    public function update(ImersoesRequest $request, Imersao $imersao)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Imersao::upload_imagem();

            $imersao->update($input);

            return redirect()->route('painel.imersoes.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Imersao $imersao)
    {
        try {
            $imersao->delete();

            return redirect()->route('painel.imersoes.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
