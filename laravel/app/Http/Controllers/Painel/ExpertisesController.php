<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExpertisesRequest;
use App\Models\Expertise;

class ExpertisesController extends Controller
{
    public function index()
    {
        $expertises = Expertise::ordenados()->get();

        return view('painel.empresa.expertises.index', compact('expertises'));
    }

    public function create()
    {
        return view('painel.empresa.expertises.create');
    }

    public function store(ExpertisesRequest $request)
    {
        try {
            $input = $request->all();

            Expertise::create($input);

            return redirect()->route('painel.expertises.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Expertise $expertise)
    {
        return view('painel.empresa.expertises.edit', compact('expertise'));
    }

    public function update(ExpertisesRequest $request, Expertise $expertise)
    {
        try {
            $input = $request->all();

            $expertise->update($input);

            return redirect()->route('painel.expertises.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Expertise $expertise)
    {
        try {
            $expertise->delete();

            return redirect()->route('painel.expertises.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
