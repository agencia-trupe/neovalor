<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmpresaRequest;
use App\Models\Empresa;

class EmpresaController extends Controller
{
    public function index()
    {
        $registro = Empresa::first();

        return view('painel.empresa.dados.edit', compact('registro'));
    }

    public function update(EmpresaRequest $request, Empresa $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Empresa::upload_capa();
            if (isset($input['imagem'])) $input['imagem'] = Empresa::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.empresa.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
