<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\BlogCategoriasRequest;
use App\Models\BlogCategoria;

class BlogCategoriasController extends Controller
{
    public function index()
    {
        $categorias = BlogCategoria::ordenados()->get();

        return view('painel.blog.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.blog.categorias.create');
    }

    public function store(BlogCategoriasRequest $request)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            $count = count(BlogCategoria::where('slug', $input['slug'])->get());

            if ($count == 0) {
                BlogCategoria::create($input);
            } else {
                return back()->withErrors(['Erro ao adicionar registro: Verifique se esse registro já existe']);
            }

            return redirect()->route('painel.blog.categorias.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit($categoria_id)
    {
        $categoria = BlogCategoria::where('id', $categoria_id)->first();

        return view('painel.blog.categorias.edit', compact('categoria'));
    }

    public function update(BlogCategoriasRequest $request, $categoria_id)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            $categoria = BlogCategoria::where('id', $categoria_id)->first();

            $categoria->update($input);

            return redirect()->route('painel.blog.categorias.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy($categoria_id)
    {
        try {
            $categoria = BlogCategoria::where('id', $categoria_id)->first();

            $categoria->delete();

            return redirect()->route('painel.blog.categorias.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
