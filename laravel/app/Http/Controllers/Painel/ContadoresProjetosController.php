<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContadoresProjetosRequest;
use App\Models\ContadorProjetos;

class ContadoresProjetosController extends Controller
{
    public function index()
    {
        $registro = ContadorProjetos::first();

        return view('painel.clientes-e-projetos.contadores.edit', compact('registro'));
    }

    public function update(ContadoresProjetosRequest $request, $registro_id)
    {
        try {
            $input = $request->all();

            $registro = ContadorProjetos::where('id', $registro_id)->first();

            $registro->update($input);

            return redirect()->route('painel.clientes-e-projetos.contadores.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
