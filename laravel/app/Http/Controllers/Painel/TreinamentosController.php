<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\TreinamentosRequest;
use App\Models\Treinamento;

class TreinamentosController extends Controller
{
    public function index()
    {
        $treinamentos = Treinamento::ordenados()->get();

        return view('painel.clientes-e-projetos.treinamentos.index', compact('treinamentos'));
    }

    public function create()
    {
        return view('painel.clientes-e-projetos.treinamentos.create');
    }

    public function store(TreinamentosRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Treinamento::upload_imagem();

            Treinamento::create($input);

            return redirect()->route('painel.treinamentos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Treinamento $treinamento)
    {
        return view('painel.clientes-e-projetos.treinamentos.edit', compact('treinamento'));
    }

    public function update(TreinamentosRequest $request, Treinamento $treinamento)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Treinamento::upload_imagem();

            $treinamento->update($input);

            return redirect()->route('painel.treinamentos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Treinamento $treinamento)
    {
        try {
            $treinamento->delete();

            return redirect()->route('painel.treinamentos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
