<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExperienciasRequest;
use App\Http\Requests\ExpertisesRequest;
use App\Models\Experiencia;
use App\Models\GrupoExperiencias;

class ExperienciasController extends Controller
{
    public function index()
    {
        $grupos = GrupoExperiencias::ordenados()->get();

        if (isset($_GET['grupo'])) {
            $grupo = $_GET['grupo'];
            $experiencias = Experiencia::grupo($grupo)->ordenados()->get();
        } else {
            $experiencias = Experiencia::ordenados()->get();
        }        

        return view('painel.clientes-e-projetos.experiencias.index', compact('experiencias', 'grupos'));
    }

    public function create()
    {
        $grupos = GrupoExperiencias::ordenados()->pluck('titulo', 'id');

        return view('painel.clientes-e-projetos.experiencias.create', compact('grupos'));
    }

    public function store(ExperienciasRequest $request)
    {
        try {
            $input = $request->all();

            Experiencia::create($input);

            return redirect()->route('painel.experiencias.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Experiencia $experiencia)
    {
        $grupos = GrupoExperiencias::ordenados()->pluck('titulo', 'id');

        return view('painel.clientes-e-projetos.experiencias.edit', compact('experiencia', 'grupos'));
    }

    public function update(ExperienciasRequest $request, Experiencia $experiencia)
    {
        try {
            $input = $request->all();

            $experiencia->update($input);

            return redirect()->route('painel.experiencias.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Experiencia $experiencia)
    {
        try {
            $experiencia->delete();

            return redirect()->route('painel.experiencias.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
