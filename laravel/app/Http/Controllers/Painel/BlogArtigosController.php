<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\BlogArtigosRequest;
use App\Models\BlogArtigo;
use App\Models\BlogCategoria;

class BlogArtigosController extends Controller
{
    public function index()
    {
        $categorias = BlogCategoria::ordenados()->get();

        if (isset($_GET['categoria'])) {
            $categoria = $_GET['categoria'];
            $artigos = BlogArtigo::categoria($categoria)->orderBy('data', 'desc')->get();
        } else {
            $artigos = BlogArtigo::orderBy('data', 'desc')->get();
        }

        return view('painel.blog.artigos.index', compact('artigos', 'categorias'));
    }

    public function create()
    {
        $categorias = BlogCategoria::ordenados()->pluck('titulo', 'id');

        return view('painel.blog.artigos.create', compact('categorias'));
    }

    public function store(BlogArtigosRequest $request)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            if (isset($input['capa'])) $input['capa'] = BlogArtigo::upload_capa();

            $count = count(BlogArtigo::where('slug', $input['slug'])->where('categoria_id', $input['categoria_id'])->get());

            if ($count == 0) {
                BlogArtigo::create($input);
            } else {
                return back()->withErrors(['Erro ao adicionar registro: Verifique se esse registro já existe']);
            }

            return redirect()->route('painel.blog.artigos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit($artigo_id)
    {
        $artigo = BlogArtigo::where('id', $artigo_id)->first();
        $categorias = BlogCategoria::ordenados()->pluck('titulo', 'id');

        return view('painel.blog.artigos.edit', compact('artigo', 'categorias'));
    }

    public function update(BlogArtigosRequest $request, $artigo_id)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            $artigo = BlogArtigo::where('id', $artigo_id)->first();

            if (isset($input['capa'])) $input['capa'] = BlogArtigo::upload_capa();

            $artigo->update($input);

            return redirect()->route('painel.blog.artigos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy($artigo_id)
    {
        try {
            $artigo = BlogArtigo::where('id', $artigo_id)->first();
            $artigo->delete();

            return redirect()->route('painel.blog.artigos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
