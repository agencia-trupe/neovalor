<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ImersaoInformacoesRequest;
use App\Models\ImersaoInformacao;

class ImersaoInformacoesController extends Controller
{
    public function index()
    {
        $infos = ImersaoInformacao::ordenados()->get();

        return view('painel.imersao.informacoes.index', compact('infos'));
    }

    public function create()
    {
        return view('painel.imersao.informacoes.create');
    }

    public function store(ImersaoInformacoesRequest $request)
    {
        try {
            $input = $request->all();

            ImersaoInformacao::create($input);

            return redirect()->route('painel.imersao.informacoes.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit($info_id)
    {
        $info = ImersaoInformacao::where('id', $info_id)->first();

        return view('painel.imersao.informacoes.edit', compact('info'));
    }

    public function update(ImersaoInformacoesRequest $request, $info_id)
    {
        try {
            $input = $request->all();

            $info = ImersaoInformacao::where('id', $info_id)->first();

            $info->update($input);

            return redirect()->route('painel.imersao.informacoes.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy($info_id)
    {
        try {
            $info = ImersaoInformacao::where('id', $info_id)->first();

            $info->delete();

            return redirect()->route('painel.imersao.informacoes.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
