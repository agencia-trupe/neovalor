<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContadoresHomeRequest;
use App\Models\ContadorHome;

class ContadoresHomeController extends Controller
{
    public function index()
    {
        $registro = ContadorHome::first();

        return view('painel.home.contadores.edit', compact('registro'));
    }

    public function update(ContadoresHomeRequest $request, $registro_id)
    {
        try {
            $input = $request->all();

            $registro = ContadorHome::where('id', $registro_id)->first();
            
            $registro->update($input);

            return redirect()->route('painel.home.contadores.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
