<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\CurriculoRequest;
use App\Models\Curriculo;

class CurriculoController extends Controller
{
    public function index()
    {
        $registro = Curriculo::first();

        return view('painel.empresa.curriculo.edit', compact('registro'));
    }

    public function update(CurriculoRequest $request, Curriculo $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['foto'])) $input['foto'] = Curriculo::upload_foto();

            $registro->update($input);

            return redirect()->route('painel.curriculo.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
