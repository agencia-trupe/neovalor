<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('neovalor', 'NeovalorController@index')->name('neovalor');
    Route::get('neovalor/curriculo', 'NeovalorController@indexCurriculo')->name('curriculo');
    Route::get('consultoria', 'ConsultoriaController@index')->name('consultoria');
    Route::get('clientes-e-projetos', 'ClientesProjetosController@index')->name('clientes-e-projetos');
    Route::get('educacao-corporativa', 'ImersaoController@index')->name('imersao'); // alterou nome para EDUCAÇÃO CORPORATIVA
    Route::get('neolabx', 'NeolabxController@index')->name('neolabx');
    Route::get('neolabx/rede-de-consultores', 'NeolabxController@indexRede')->name('neolabx.rede');
    Route::get('blog', 'BlogController@index')->name('blog');
    Route::get('blog/{artigo}', 'BlogController@show')->name('blog.show');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');
    Route::get('politica-de-privacidade', 'PoliticaDePrivacidadeController@index')->name('politica-de-privacidade');
    Route::post('aceite-de-cookies', 'HomeController@postCookies')->name('aceite-de-cookies.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function () {
        Route::get('/', 'PainelController@index')->name('painel');
        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::resource('home', 'HomeController', ['only' => ['index', 'update']]);
        Route::resource('banners', 'BannersController');
        Route::resource('home/contadores', 'ContadoresHomeController', ['only' => ['index', 'update']]);
        Route::resource('empresa', 'EmpresaController', ['only' => ['index', 'update']]);
        Route::resource('expertises', 'ExpertisesController');
        Route::resource('curriculo', 'CurriculoController', ['only' => ['index', 'update']]);
        Route::resource('consultoria', 'ConsultoriaController', ['only' => ['index', 'update']]);
        Route::resource('projetos', 'ProjetosController');
        Route::resource('consultoria/informacoes', 'ConsultoriaInformacoesController');
        Route::resource('clientes-e-projetos', 'ClientesProjetosController', ['only' => ['index', 'update']]);
        Route::resource('clientes-e-projetos/contadores', 'ContadoresProjetosController', ['only' => ['index', 'update']]);
        Route::resource('grupos-experiencias', 'GruposExperienciasController');
        Route::resource('experiencias', 'ExperienciasController');
        Route::resource('treinamentos', 'TreinamentosController');
        Route::resource('imersao', 'ImersaoDadosController', ['only' => ['index', 'update']]);
        Route::resource('imersoes', 'ImersoesController');
        Route::resource('imersao/informacoes', 'ImersaoInformacoesController');
        Route::resource('neolabx', 'NeolabxController', ['only' => ['index', 'update']]);
        Route::resource('neolabx/inovacao', 'NeolabxInovacaoController', ['only' => ['index', 'edit', 'update']]);
        Route::resource('neolabx/consultores', 'NeolabxConsultoresController');
        Route::resource('blog/categorias', 'BlogCategoriasController');
        Route::resource('blog/artigos', 'BlogArtigosController');
        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('politica-de-privacidade', 'PoliticaDePrivacidadeController', ['only' => ['index', 'update']]);
        Route::get('aceite-de-cookies', 'AceiteDeCookiesController@index')->name('painel.aceite-de-cookies');
        Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);
        Route::resource('usuarios', 'UsuariosController');

        // Limpar caches
        Route::get('clear-cache', function() {
            $exitCode = Artisan::call('config:clear');
            $exitCode = Artisan::call('cache:clear');
            $exitCode = Artisan::call('route:clear');
            $exitCode = Artisan::call('view:clear');
            $exitCode = Artisan::call('config:cache');

            return 'DONE';
        });
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function () {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
        Route::get('esqueci-minha-senha', 'PasswordController@sendEmail')->name('esqueci-minha-senha');
        Route::post('esqueci-minha-senha', 'PasswordController@sendResetLinkEmail')->name('esqueci-minha-senha.post');
        Route::get('reset/{token}', 'PasswordController@showResetForm')->name('password-reset');
        Route::post('reset', 'PasswordController@reset')->name('password-reset.post');
    });
});
