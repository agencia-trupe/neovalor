<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TreinamentosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem'    => 'required|image',
            'empresa'   => 'required',
            'ano'       => 'required',
            'tipo'      => 'required',
            'titulo'    => 'required',
            'consultor' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
        ];
    }
}
