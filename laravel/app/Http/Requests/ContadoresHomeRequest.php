<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContadoresHomeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'item1'       => 'required',
            'quantidade1' => 'required',
            'item2'       => 'required',
            'quantidade2' => 'required',
            'item3'       => 'required',
            'quantidade3' => 'required',
            'item4'       => 'required',
            'quantidade4' => 'required',
            'item5'       => 'required',
            'quantidade5' => 'required',
            'item6'       => 'required',
            'quantidade6' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
        ];
    }
}
