<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ConsultoriaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'capa'              => 'required|image',
            'titulo'            => 'required',
            'texto_consultoria' => 'required',
            'texto_beneficios'  => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
        ];
    }
}
