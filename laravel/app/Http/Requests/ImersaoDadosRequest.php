<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ImersaoDadosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'capa'               => 'required|image',
            'titulo'             => 'required',
            'texto_imersao'      => 'required',
            'texto_diferenciais' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
        ];
    }
}
