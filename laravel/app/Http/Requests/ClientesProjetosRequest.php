<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ClientesProjetosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'capa'               => 'required|image',
            'frase'              => 'required',
            'texto_treinamentos' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
        ];
    }
}
