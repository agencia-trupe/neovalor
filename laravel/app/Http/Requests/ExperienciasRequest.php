<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ExperienciasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'periodo' => 'required',
            'titulo'  => 'required',
            'texto'   => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
        ];
    }
}
