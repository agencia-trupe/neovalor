<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NeolabxConsultoresRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'foto'          => 'required|image',
            'nome'          => 'required',
            'texto'         => 'required',
            'email'         => '',
            'whatsapp'      => '',
            'linkedin_link' => '',
            'linkedin_nome' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['foto'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
        ];
    }
}
