<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContatoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'          => 'required',
            'email'         => 'required|email',
            'celular'       => 'required',
            'whatsapp'      => 'required',
            'atendimento'   => 'required',
            'facebook'      => 'required',
            'linkedin'      => 'required',
            'tipo_trabalho' => 'required',
            'endereco'      => 'required',
            'bairro'        => 'required',
            'cidade'        => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
            'email'    => "Insira um endereço de e-mail válido.",
        ];
    }
}
