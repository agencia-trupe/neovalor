<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CurriculoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome'   => 'required',
            'foto'   => 'required|image',
            'titulo' => 'required',
            'texto'  => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['foto'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
        ];
    }
}
