<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EmpresaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'capa'        => 'required|image',
            'imagem'      => 'required|image',
            'sobre'       => 'required',
            'expertises'  => 'required',
            'metodologia' => 'required',
            'missao'      => 'required',
            'visao'       => 'required',
            'proposito'   => 'required',
            'valores'     => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
            $rules['imagem'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
        ];
    }
}
