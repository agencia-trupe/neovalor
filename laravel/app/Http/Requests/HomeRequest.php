<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HomeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'texto_sobre'    => 'required',
            'texto_projetos' => 'required',
            'frase_cx1'      => 'required',
            'link_cx1'       => 'required',
            'frase_cx2'      => 'required',
            'link_cx2'       => 'required',
            'frase_cx3'      => 'required',
            'link_cx3'       => 'required',
            'imagem_bg'      => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem_bg'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
        ];
    }
}
