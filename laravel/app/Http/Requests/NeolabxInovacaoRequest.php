<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NeolabxInovacaoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem'    => 'required|image',
            'titulo'    => 'required',
            'subtitulo' => 'required',
            'texto'     => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
        ];
    }
}
