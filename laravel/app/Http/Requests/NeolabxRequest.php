<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NeolabxRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo'     => 'required',
            'capa'       => 'required|image',
            'img_rede'   => 'required|image',
            'frase_pt1'  => 'required',
            'frase_pt2'  => 'required',
            'item1'      => 'required',
            'item2'      => 'required',
            'item3'      => 'required',
            'texto_rede' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
            $rules['img_rede'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
        ];
    }
}
