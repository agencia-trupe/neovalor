<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateEmpresaTable extends Migration
{
    public function up()
    {
        Schema::create('empresa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('capa');
            $table->text('sobre');
            $table->text('expertises');
            $table->text('metodologia');
            $table->string('imagem');
            $table->text('missao');
            $table->text('visao');
            $table->text('proposito');
            $table->text('valores');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empresa');
    }
}
