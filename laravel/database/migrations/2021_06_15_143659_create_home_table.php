<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateHomeTable extends Migration
{
    public function up()
    {
        Schema::create('home', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto_sobre');
            $table->text('texto_projetos');
            $table->string('frase_cx1');
            $table->string('link_cx1');
            $table->string('frase_cx2');
            $table->string('link_cx2');
            $table->string('frase_cx3');
            $table->string('link_cx3');
            $table->string('imagem_bg');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('home');
    }
}
