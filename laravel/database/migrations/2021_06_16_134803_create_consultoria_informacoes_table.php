<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateConsultoriaInformacoesTable extends Migration
{
    public function up()
    {
        Schema::create('consultoria_informacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->text('texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('consultoria_informacoes');
    }
}
