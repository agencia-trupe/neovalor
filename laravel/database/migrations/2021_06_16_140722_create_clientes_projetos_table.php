<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateClientesProjetosTable extends Migration
{
    public function up()
    {
        Schema::create('clientes_projetos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('capa');
            $table->string('frase');
            $table->text('texto_treinamentos');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('clientes_projetos');
    }
}
