<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateContatoTable extends Migration
{
    public function up()
    {
        Schema::create('contato', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('email');
            $table->string('celular');
            $table->string('whatsapp');
            $table->string('atendimento');
            $table->string('facebook');
            $table->string('linkedin');
            $table->string('tipo_trabalho');
            $table->string('endereco');
            $table->string('bairro');
            $table->string('cidade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('contato');
    }
}
