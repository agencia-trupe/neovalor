<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateConsultoriaTable extends Migration
{
    public function up()
    {
        Schema::create('consultoria', function (Blueprint $table) {
            $table->increments('id');
            $table->string('capa');
            $table->string('titulo');
            $table->text('texto_consultoria');
            $table->text('texto_beneficios');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('consultoria');
    }
}
