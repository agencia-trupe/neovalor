<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateConfiguracoesTable extends Migration
{
    public function up()
    {
        Schema::create('configuracoes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->text('keywords');
            $table->string('imagem_de_compartilhamento');
            $table->string('analytics_ua')->nullable();
            $table->string('analytics_g')->nullable();
            $table->string('codigo_gtm')->nullable();
            $table->string('pixel_facebook')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('configuracoes');
    }
}
