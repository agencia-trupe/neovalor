<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateImersaoDadosTable extends Migration
{
    public function up()
    {
        Schema::create('imersao_dados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('capa');
            $table->string('titulo');
            $table->text('texto_imersao');
            $table->text('texto_diferenciais');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('imersao_dados');
    }
}
