<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateNeolabxInovacaoTable extends Migration
{
    public function up()
    {
        Schema::create('neolabx_inovacao', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem');
            $table->string('slug');
            $table->string('titulo');
            $table->string('subtitulo');
            $table->text('texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('neolabx_inovacao');
    }
}
