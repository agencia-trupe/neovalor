<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateBlogArtigosTable extends Migration
{
    public function up()
    {
        Schema::create('blog_artigos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('categoria_id')->unsigned();
            $table->foreign('categoria_id')->references('id')->on('blog_categorias')->onDelete('cascade');
            $table->string('capa');
            $table->string('slug');
            $table->string('titulo');
            $table->string('autor');
            $table->date('data');
            $table->text('texto');
            $table->boolean('neolabx')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('blog_artigos');
    }
}
