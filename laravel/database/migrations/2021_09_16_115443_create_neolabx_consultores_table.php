<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateNeolabxConsultoresTable extends Migration
{
    public function up()
    {
        Schema::create('neolabx_consultores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('foto');
            $table->string('nome');
            $table->text('texto');
            $table->string('email')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('linkedin_link')->nullable();
            $table->string('linkedin_nome')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('neolabx_consultores');
    }
}
