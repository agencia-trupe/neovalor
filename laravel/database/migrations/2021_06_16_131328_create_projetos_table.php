<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateProjetosTable extends Migration
{
    public function up()
    {
        Schema::create('projetos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('icone');
            $table->string('titulo');
            $table->text('texto');
            $table->boolean('novidade')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('projetos');
    }
}
