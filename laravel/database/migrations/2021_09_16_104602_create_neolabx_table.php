<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateNeolabxTable extends Migration
{
    public function up()
    {
        Schema::create('neolabx', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->string('capa');
            $table->string('img_rede');
            $table->string('frase_pt1');
            $table->string('frase_pt2');
            $table->string('item1');
            $table->string('item2');
            $table->string('item3');
            $table->text('texto_rede');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('neolabx');
    }
}
