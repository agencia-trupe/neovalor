<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateTreinamentosTable extends Migration
{
    public function up()
    {
        Schema::create('treinamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('empresa');
            $table->string('ano');
            $table->string('tipo');
            $table->string('titulo');
            $table->string('consultor');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('treinamentos');
    }
}
