<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateCurriculoTable extends Migration
{
    public function up()
    {
        Schema::create('curriculo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('foto');
            $table->string('titulo');
            $table->text('texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('curriculo');
    }
}
