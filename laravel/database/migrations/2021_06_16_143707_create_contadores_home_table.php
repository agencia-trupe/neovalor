<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateContadoresHomeTable extends Migration
{
    public function up()
    {
        Schema::create('contadores_home', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item1');
            $table->integer('quantidade1');
            $table->string('item2');
            $table->integer('quantidade2');
            $table->string('item3');
            $table->integer('quantidade3');
            $table->string('item4');
            $table->integer('quantidade4');
            $table->string('item5');
            $table->integer('quantidade5');
            $table->string('item6');
            $table->integer('quantidade6');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('contadores_home');
    }
}
