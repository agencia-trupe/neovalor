<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientesProjetosTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('clientes_projetos')->insert([
            'capa'               => '',
            'frase'              => 'Nesses 13 anos - desde 2008 - tenho atuado em diversos tipos de trabalhos e projetos:',
            'texto_treinamentos' => '',
        ]);
    }
}
