<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CurriculoTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('curriculo')->insert([
            'nome'   => 'Sérgio Lima',
            'foto'   => '',
            'titulo' => 'Consultor Gestão Empresarial - Melhoria de Desempenho Organizacional',
            'texto'  => '',
        ]);
    }
}
