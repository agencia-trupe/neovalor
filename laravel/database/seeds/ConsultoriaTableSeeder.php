<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConsultoriaTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('consultoria')->insert([
            'capa'              => '',
            'titulo'            => 'Serviços de consultoria',
            'texto_consultoria' => '',
            'texto_beneficios'  => '',
        ]);
    }
}
