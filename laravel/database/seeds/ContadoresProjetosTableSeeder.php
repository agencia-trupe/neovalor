<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContadoresProjetosTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contadores_projetos')->insert([
            'item1'       => 'Experiência Empreendedora',
            'quantidade1' => 1,
            'item2'       => 'Gestões Interinas / PJ',
            'quantidade2' => 2,
            'item3'       => 'Avaliações de Negócios',
            'quantidade3' => 4,
            'item4'       => 'Projetos de Consultoria Empresarial',
            'quantidade4' => 6,
            'item5'       => 'Projetos: Líder de Mudança Customizados',
            'quantidade5' => 2,
            'item6'       => 'Projetos de Mentoring',
            'quantidade6' => 1,
        ]);
    }
}
