<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GruposExperienciasTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('grupos_experiencias')->insert([
            'id'     => 1,
            'ordem'  => 1,
            'titulo' => 'Experiência Empreendora',
        ]);

        DB::table('grupos_experiencias')->insert([
            'id'     => 2,
            'ordem'  => 2,
            'titulo' => 'Gestão PJ',
        ]);

        DB::table('grupos_experiencias')->insert([
            'id'     => 3,
            'ordem'  => 3,
            'titulo' => 'Elaboração de Planos de Negócios',
        ]);

        DB::table('grupos_experiencias')->insert([
            'id'     => 4,
            'ordem'  => 4,
            'titulo' => 'Projetos de Consultoria Empresarial',
        ]);

        DB::table('grupos_experiencias')->insert([
            'id'     => 5,
            'ordem'  => 5,
            'titulo' => 'Projetos Liderar Mudanças - customizados',
        ]);

        DB::table('grupos_experiencias')->insert([
            'id'     => 6,
            'ordem'  => 6,
            'titulo' => 'Mentoring',
        ]);
    }
}
