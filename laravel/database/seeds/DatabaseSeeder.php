<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ConfiguracoesTableSeeder::class);
        $this->call(PoliticaDePrivacidadeTableSeeder::class);
        $this->call(ContatoTableSeeder::class);
        $this->call(HomeTableSeeder::class);
        $this->call(EmpresaTableSeeder::class);
        $this->call(CurriculoTableSeeder::class);
        $this->call(ConsultoriaTableSeeder::class);
        $this->call(ClientesProjetosTableSeeder::class);
        $this->call(ContadoresHomeTableSeeder::class);
        $this->call(ContadoresProjetosTableSeeder::class);
        $this->call(GruposExperienciasTableSeeder::class);
        $this->call(ImersaoDadosTableSeeder::class);
        $this->call(NeolabxTableSeeder::class);
        $this->call(NeolabxInovacaoTableSeeder::class);
    }
}
