<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NeolabxTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('neolabx')->insert([
            'titulo'     => 'A evolução da Consultoria',
            'capa'       => '',
            'img_rede'   => '',
            'frase_pt1'  => 'Você decide como quer melhorar o seu desempenho e o da sua Empresa',
            'frase_pt2'  => 'Uma Rede multidisciplinar de Consultores pronta para ouvi-lo',
            'item1'      => 'Novos conceitos - Novas possibilidades',
            'item2'      => 'O diferencial do NeoLabX é você criar alternativas para ser atendido e efetivar um projeto de melhoria de desempenho',
            'item3'      => 'Um espaço inovador para atendê-lo',
            'texto_rede' => '',
        ]);
    }
}
