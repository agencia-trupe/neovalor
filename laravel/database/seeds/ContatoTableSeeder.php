<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContatoTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contato')->insert([
            'nome'          => 'Neovalor',
            'email'         => 'contato@trupe.net',
            'celular'       => '11 94548 1001',
            'whatsapp'      => '11 94548 1001',
            'atendimento'   => 'segundas às sextas das 9h às 17h',
            'facebook'      => 'https://www.facebook.com/neovalor',
            'linkedin'      => 'https://www.linkedin.com/company/neovalor/about/',
            'tipo_trabalho' => 'Home Office',
            'endereco'      => 'Condomínio Acervo',
            'bairro'        => 'Alto de Pinheiros',
            'cidade'        => 'São Paulo · SP',
        ]);
    }
}
