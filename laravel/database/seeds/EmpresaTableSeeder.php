<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmpresaTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('empresa')->insert([
            'capa'        => '',
            'sobre'       => '',
            'expertises'  => '',
            'metodologia' => '',
            'imagem'      => '',
            'missao'      => '',
            'visao'       => '',
            'proposito'   => '',
            'valores'     => '',
        ]);
    }
}
