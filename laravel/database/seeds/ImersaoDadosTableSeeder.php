<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ImersaoDadosTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('imersao_dados')->insert([
            'capa'               => '',
            'titulo'             => 'Imersões',
            'texto_imersao'      => '',
            'texto_diferenciais' => '',
        ]);
    }
}
