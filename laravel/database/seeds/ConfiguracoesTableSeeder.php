<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfiguracoesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('configuracoes')->insert([
            'title'                      => 'Neovalor',
            'description'                => '',
            'keywords'                   => '',
            'imagem_de_compartilhamento' => '',
            'analytics_ua'               => '',
            'analytics_g'                => '',
            'codigo_gtm'                 => '',
            'pixel_facebook'             => '',
        ]);
    }
}
