<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NeolabxInovacaoTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('neolabx_inovacao')->insert([
            'id'        => 1,
            'imagem'    => '',
            'slug'      => 'neo-interacao',
            'titulo'    => 'NEO INTERAÇÃO',
            'subtitulo' => 'Você direciona',
            'texto'     => '',
        ]);

        DB::table('neolabx_inovacao')->insert([
            'id'        => 2,
            'imagem'    => '',
            'slug'      => 'neo-proposito',
            'titulo'    => 'NEO PROPÓSITO',
            'subtitulo' => 'Você atendido',
            'texto'     => '',
        ]);

        DB::table('neolabx_inovacao')->insert([
            'id'        => 3,
            'imagem'    => '',
            'slug'      => 'neo-resultado',
            'titulo'    => 'NEO RESULTADO',
            'subtitulo' => 'Você participa',
            'texto'     => '',
        ]);
    }
}
