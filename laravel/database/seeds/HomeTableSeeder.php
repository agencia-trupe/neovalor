<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HomeTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('home')->insert([
            'texto_sobre'    => '',
            'texto_projetos' => '',
            'frase_cx1'      => 'Conheça mais sobre o que proponho, meu histórico, valores e propostas de trabalho.',
            'link_cx1'       => '',
            'frase_cx2'      => 'Desenvolvo projetos de consultoria em gestão empresarial. Conheça as expertises, diferenciais e metodologia.',
            'link_cx2'       => '',
            'frase_cx3'      => 'Através de imersões vamos exercitar novos conhecimentos que geram ações de melhoria.',
            'link_cx3'       => '',
            'imagem_bg'      => '',
        ]);
    }
}
