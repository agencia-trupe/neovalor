<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContadoresHomeTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contadores_home')->insert([
            'item1'       => 'Projetos de Consultoria',
            'quantidade1' => 6,
            'item2'       => 'Projetos de Gestão Interina / PJ',
            'quantidade2' => 2,
            'item3'       => 'Avaliações de Negócios',
            'quantidade3' => 4,
            'item4'       => 'Projetos de Liderar Mudanças',
            'quantidade4' => 2,
            'item5'       => 'Projetos de Mentoring',
            'quantidade5' => 1,
            'item6'       => 'Experiência Empreendedora',
            'quantidade6' => 3,
        ]);
    }
}
